/**************************************************************************************************************************************
 * <VARIABLES GLOBALES>
**************************************************************************************************************************************/
//#region
var cTituloModal    	= "Menu Afore";
var cMensaje        	= "";
var ligaCase        	= "php/casebase.php";
var iEmpleadoCapt 		= 0;
var iEmpleado       	= 0;
var sContrasenia		= "";
var iTienda				= 0;
var sPasssaftv			= "";
var sClaveconsar		= "";
var ipSPA           	= "10.0.0.1";
var ipModulo        	= "10.0.0.1";
var idVideo 			= 0;
var bOpciones 			= false;
var arrOpcionesMenu 	= new Array();
var iTiempoMonitor 		= 0;
//#endregion

/**************************************************************************************************************************************
 * <VARIABLES EXES>
**************************************************************************************************************************************/
//#region
//OPCIONES EJECUTABLES
iOpcionWebx 			= 0;
HE						= 1;
NAVEGADOR_FIREFOX		= 2;
NAVEGADOR_HTMLLOGIN		= 3;
NAVEGADOR_NAVIUS		= 4;
APAGARPC				= 5;
//RUTAS
RUTA_HE					= "";
RUTA_FIREFOX 			= "";
RUTA_HTMLLOGIN 			= "";
RUTA_NAVIUS 			= "";
RUTA_APAGARPC			= "";
PARAM_APAGARPC 			= "";
//#endregion

/**************************************************************************************************************************************
 * <INICIO>
**************************************************************************************************************************************/
//#region
$(document).ready(function () {
    datosPagina();
	console.log("ipSPA-> " + ipSPA + " ipModulo-> " +  ipModulo);
	menuEjecutables();

	//Regresar al Menu
	$("#logOutEvent").click(function(){
		bOpciones = false;
		$(".cont-user").hide();
		$("#divBodPag").html("");
		$("#divBodSol").html("");
		$("#divBodSer").html("");
		$("#divBodOtr").html("");
		$(".divMenu").hide();
		$(".divAccesoMenu").show();
		$("#ipEmpleado").val("");
		$("#ipEmpleado").prop("disabled", false);
		resizeContent();
	});

	//Validar numero de empleado
	$("#btnEntrar").click(function(){
		mdlEspere(cTituloModal);
		validarTxtEmpleado();
	});

	//Detonar apagado de PC
	$("#btnApagar").click(function(){
		sMensaje 	= "¿Confirmar APAGADO de computadora?";
		sBtn1 		= "SI";
		fFunc1 		= "opcionEjecuta(" + APAGARPC + "," + PARAM_APAGARPC + ");";
		sBtn2 		= "NO";
		fFunc2 		= "mdlDesbloquearMsj();";
		mdlMsjFuncPreg(cTituloModal,sMensaje,sBtn1,fFunc1,sBtn2,fFunc2);
	});

	$('#ipEmpleado').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){ // key enter
			mdlEspere(cTituloModal);
			validarTxtEmpleado();
		}
	});

	//Obtener mensajes
	$("#divBtnHist").click(function(){
		obtenerMenuMensajes(2);
	});

	//Redimenzionar pantalla
	$(window).resize(function() {
		if(bOpciones)
			resizeContent();
    });

});
//#endregion

function resizeContent() {
	var heightWindow 	= $(window).height();
	var heightHeader 	= $("header").height();
	var heightTotal 	= heightWindow - (heightHeader);
	var heightOpciones 	= heightTotal * 0.65;
	var heightHistorial = heightTotal * 0.28;
	$("#divOpciones").height(heightOpciones);
	$("#divMensajes").height(heightHistorial);
}

/**************************************************************************************************************************************
 * <FUNCIONES ACCESO>
**************************************************************************************************************************************/
//#region
function grabarRegistro(mensaje) {
    $.ajax({
        async: false,
        cache: false,
        url: ligaCase,
        type: "POST",
        dataType: "JSON",
        data: { opcion: 1, mensaje: mensaje },
        success: function (data) {
            console.log(data);
        },error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }
    });
}

function datosPagina() {
    $.ajax({
        async: false,
        cache: false,
        url: ligaCase,
        type: "POST",
        dataType: "JSON",
        data: { opcion: 2},
        success: function (data) {
            console.log(data);
            if(data.irespuesta == 4){
                $("#fechaActual").text(data.fechaActual);
                ipSPA       = data.ipspa;
                ipModulo    = data.ipmodulo;
            }
        }, error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }
    });
}

function menuEjecutables(){
	var aResp = new Array();
	$.ajax({
        async: false,
        cache: false,
        url: ligaCase,
        type: "POST",
        dataType: "JSON",
        data: { opcion: 3, idcons: "CONS00"},
        success: function (data) {
            if(data.irespuesta == 4){
				aResp = data["CONS00"];
				console.log(aResp);
				$.each(aResp , function(index, val) {
					switch (parseInt(val.exeid)) {
						case HE: 					RUTA_HE 		= val.exeliga; break;
						case NAVEGADOR_FIREFOX: 	RUTA_FIREFOX 	= val.exeliga; break;
						case NAVEGADOR_HTMLLOGIN: 	RUTA_HTMLLOGIN 	= val.exeliga; break;
						case NAVEGADOR_NAVIUS: 		RUTA_NAVIUS 	= val.exeliga; break;
						case APAGARPC: 				RUTA_APAGARPC 	= val.exeliga; PARAM_APAGARPC = val.exeparametros; break;
						default: 
							if(parseInt(val.exeestatus) == 1){
								console.log("LIGA-> " + val.exeliga + " PARAMETROS->" + val.exeparametros);
								opcionEjecuta(val.exeliga,val.exeparametros); 
							}
						break;
					}
				});
            }
        }, error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }
	});
}
//#endregion

/**************************************************************************************************************************************
 * <DETONACION DE EXES Y RESPUESTAS>
**************************************************************************************************************************************/
//#region
function opcionEjecuta(opcion,parametros) {
    iOpcion         = opcion;
	var sRuta       = "";
	var espera		= 2;

    switch (iOpcion) {
		case HE: 					sRuta = RUTA_HE; espera = 1; 	break;
		case NAVEGADOR_FIREFOX: 	sRuta = RUTA_FIREFOX; 			break;
		case NAVEGADOR_HTMLLOGIN: 	sRuta = RUTA_HTMLLOGIN; 		break;
		case NAVEGADOR_NAVIUS: 		sRuta = RUTA_NAVIUS; 			break;
		case APAGARPC: 				sRuta = RUTA_APAGARPC; 			break;
		default: 					sRuta = opcion; 				break;
    }
    ejecutaWebService(sRuta, parametros, espera);
}

function ejecutaWebService(sRuta, sParametros, iEspera) {
	console.log("iEspera-> " + iEspera + " sRuta-> " + sRuta + " sParametros-> " + sParametros);
	soapData 	= "",
	httpObject 	= null,
	docXml 		= null,
	iEstado 	= 0,
	sMensaje 	= "";
	sUrlSoap 	= "http://127.0.0.1:20044/";
	soapData 	=
		'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
		'<SOAP-ENV:Envelope' +
		'	xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"' +
		'	xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"' +
		'	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"' +
		'	xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"' +
		'	xmlns:ns2=\"urn:ServiciosWebx\">' +
		'	<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">' +
		'		<ns2:ejecutarAplicacion>' +
		'			<inParam>' +
		'				<Esperar> ' + iEspera + ' </Esperar>' +
		'				<RutaAplicacion>' + sRuta + '</RutaAplicacion>' +
		'				<parametros><![CDATA[' + sParametros + ']]></parametros>' +
		'			</inParam>' +
		'		</ns2:ejecutarAplicacion>' +
		'	</SOAP-ENV:Body>' +
		'</SOAP-ENV:Envelope>';
	httpObject = getHTTPObject();
	if (httpObject) {
		if (httpObject.overrideMimeType) {
			httpObject.overrideMimeType("false");
		}
		httpObject.open("POST", sUrlSoap, false); //-- no asincrono
		httpObject.setRequestHeader("Accept-Language", null);
		httpObject.onreadystatechange = function () {
			if (httpObject.readyState == 4 && httpObject.status == 200) {
				parser = new DOMParser();
				docXml = parser.parseFromString(httpObject.responseText, "text/xml");
				iEstado = docXml.getElementsByTagName("Estado")[0].childNodes[0].nodeValue;
				console.log("Estado:",iEstado);
				//Llamada a la función que recibe la respuesta del WebService
				respuestaWebService(parseInt(iEstado));
			}
			else {
				console.log(httpObject.status);
			}
		};
		httpObject.send(soapData);
	}
}

function getHTTPObject() {
	var xhr = false;
	if (window.ActiveXObject) {
		try { xhr = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch (e) {
			try { xhr = new ActiveXObject("Microsoft.XMLHTTP"); }
			catch (e) { xhr = false; }
		}
	} else if (window.XMLHttpRequest) {
		try { xhr = new XMLHttpRequest(); }
		catch (e) { xhr = false; }
	}
	return xhr;
}

function respuestaWebService(iRespuesta) {
	console.log("iopcion -> " + iOpcion + " irespuesta -> " + iRespuesta);
	switch (iOpcion) {
		case HE:
			var bMensaje = true;
			console.log("iEmpleadoCapt-> " + iEmpleadoCapt + " iRespuesta-> " + iRespuesta);
			if(iRespuesta>=90000000) {
				if(iEmpleadoCapt == iRespuesta) {
					if(consultarEmpleado(iEmpleadoCapt)){
						bMensaje = false;
						$(".divAccesoMenu").hide();
						//Valida si el video ya se ejecuto
						if(validaReproduccionVideo(1)){
							obtenerMenuMensajes(1);
							obtenerMenuOpciones();
						}else{
							verVideo();
						}
					}else{
						cMensaje = "EL NÚMERO DE EMPLEADO REGISTRADO NO COINCIDE CON EL DE LA HUELLA";
					}
				}else{
					cMensaje = "EL NÚMERO DE EMPLEADO REGISTRADO NO COINCIDE CON EL DE LA HUELLA";
				}
			} else {
				switch(iRespuesta) {
				     case -1:	cMensaje = "SIN CONEXIÓN A SERVIDOR"; break;
				     case 1: 	cMensaje = "LA HUELLA DEL EMPLEADO NO COINCIDE"; break;
				     case 2: 	cMensaje = "EL EMPLEADO NO EXISTE EN la BASE DE DATOS"; break;
				     case 3: 	cMensaje = "NO ESTA INSTALADO EL SOFTWARE DEL SENSOR"; break;
				     case 4: 	cMensaje = "EXISTE UNA INSTANCIA ACTIVA"; break;
				     case 5: 	cMensaje = "LA CLAVE DEL EMPLEADO NO CORRESPONDE CON LA HUELLA"; break;
				     default: 	cMensaje = "EXCEPCION NO CONTROLADA PARA LA HUELLA-> " + iRespuesta; break;
				}
			}

			if(bMensaje){
				$("#ipEmpleado").prop("disabled", false);
				mdlMsj(cTituloModal,cMensaje);
			}
		break;
	}
}
//#endregion

/**************************************************************************************************************************************
 * <METODOS RESPUESTAS EXES>
**************************************************************************************************************************************/
//#region
function wsRespDefault(){
	mdlMsjCerrar(cTituloModal,"Error en opcion, favor de contactarce con mesa de ayuda.");
}
//#endregion

/**************************************************************************************************************************************
 * <OBTENER DATOS>
**************************************************************************************************************************************/
//#region 
function validarTxtEmpleado() {
	iEmpleadoCapt = $("#ipEmpleado").val();
	if(iEmpleadoCapt.length == 8){
		$("#ipEmpleado").prop("disabled", true);
		opcionEjecuta(HE,ipSPA);
	}else{
		iEmpleadoCapt = "";
		$("#ipEmpleado").val("");
		mdlMsj(cTituloModal,"Numero de empleado no valido");
	}
}

function consultarEmpleado(empleado){
	var bResp 	= false;
	var sNombre = "";
	var datos 	= empleado + ", '" + ipSPA + "'";
	var aResp 	= new Array();
	$.ajax({
        async: false,
        cache: false,
        url: ligaCase,
        type: "POST",
        dataType: "JSON",
        data: { opcion: 3, idcons: "CONS01", arrdatos: datos},
        success: function (data) {
            if(data.irespuesta == 4){
				aResp = data["CONS01"][0];
				$(".cont-user").show();
				iEmpleado 		= aResp.empleado;
				sContrasenia	= aResp.contrasenia;
				iTienda 		= aResp.tienda;
				sPasssaftv		= aResp.passsaftv;
				sClaveconsar	= aResp.claveconsar;
				$("#nombrePromo").text(correcionCaracteres(aResp.nombre));
				$("#clavePromo").text(sClaveconsar);
				$("#tiendaPromo").text(aResp.centro + " - " + aResp.nombrecentro);
				bResp = true;
            }
        }, error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }
	});
	return bResp;
}

function validaReproduccionVideo(opc){
	var bResp = false;
	var datos = iEmpleado + ", '" + ipModulo + "'," + opc;
	var aResp = new Array();
	$.ajax({
        async: false,
        cache: false,
        url: ligaCase,
        type: "POST",
        dataType: "JSON",
        data: { opcion: 3, idcons: "CONS02", arrdatos: datos},
        success: function (data) {
            if(data.irespuesta == 4){
				aResp = data["CONS02"][0];
				console.log(aResp);
				if(parseInt(aResp.iretorno) == 1){
					idVideo = aResp.ivideo;
				}else{
					bResp = true;
				}
            }
        }, error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }
	});
	return bResp;
}

function verVideo(){
	var aResp = new Array();
	$.ajax({
        async: false,
        cache: false,
        url: ligaCase,
        type: "POST",
        dataType: "JSON",
        data: { opcion: 3, idcons: "CONS03", arrdatos: idVideo},
        success: function (data) {
            if(data.irespuesta == 4){
				aResp = data["CONS03"][0];
				mdlDesbloquear();
				mdlVideo(cTituloModal,aResp.video);
				document.getElementById("idVideo").play();
				document.getElementById("idVideo").onended = function () { 
					validaReproduccionVideo(2);
					mdlDesbloquearIfrm();
					obtenerMenuMensajes(1);
					obtenerMenuOpciones();
				};
            }
        }, error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }
	});
}

function obtenerMenuMensajes(opc){
	var aResp = new Array();
	$.ajax({
        async: false,
        cache: false,
        url: ligaCase,
        type: "POST",
        dataType: "JSON",
        data: { opcion: 3, idcons: "CONS04", arrdatos: opc},
        success: function (data) {
            if(data.irespuesta == 4){
				aResp = data["CONS04"][0];
				$("#divHist").html(aResp.mensaje);
				if(opc == 1)
					$("#fechaHist").text(aResp.fecha);
            }
        }, error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }
	});
}

function obtenerMenuOpciones(){
	var arrAccion 	= new Array();
	bOpciones 		= true;
	$(".divMenu").show();
	resizeContent();
	arrAccion 		= validarAccesoRevalidacion(iEmpleado);
	arrOpcionesMenu = obtenerOpcionesMenu(iEmpleado,ipModulo,arrAccion.cmetodo);
	validarOpcionesMenu(arrOpcionesMenu,arrAccion);
}

function validarAccesoRevalidacion(promotor) {
	var aResp = new Array();
	$.ajax({
        async: false,
        cache: false,
        url: ligaCase,
        type: "POST",
		dataType: "JSON",
        data: { opcion: 3, idcons: "CONS05", arrdatos: promotor},
        success: function (data) {
            if(data.irespuesta == 4){
				aResp = data["CONS05"][0];
            }
        }, error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }
	});

	return aResp;
}

function obtenerOpcionesMenu(promotor,modulo,accion) {
	var aResp = new Array();
	var datos = promotor + ",'" + modulo + "','" + accion + "'";
	$.ajax({
        async: false,
        cache: false,
        url: ligaCase,
        type: "POST",
		dataType: "JSON",
        data: { opcion: 3, idcons: "CONS06", arrdatos: datos},
        success: function (data) {
            if(data.irespuesta == 4){
				aResp = data["CONS06"];
            }
        }, error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }
	});

	return aResp;
}

function validarOpcionesMenu(arrOpciones,arrAccion) {
	var sGrupo1 = "";//Páginas de Internet
	var sGrupo2 = "";//Solicitudes
	var sGrupo3 = "";//Servicios
	var sGrupo4 = "";//Otros
	var sNombre = "";
	//Distribuir las opciones para mostrar en el menu
	$.each(arrOpciones , function(index, val) {
		sNombre = correcionCaracteres(val.descripcion);
		if(val.control == 0){//Validar botones padres
			switch (parseInt(val.grupo)) {
				case 1://Páginas de Internet
					sGrupo1 += "<button class='btn btn-sm btn-Opc' onclick='ejecutarOpcion(" + index + ")'>" + sNombre + "</button>";
					break;
				case 2://Solicitudes
					sGrupo2 += "<button class='btn btn-sm btn-Opc' onclick='ejecutarOpcion(" + index + ")'>" + sNombre + "</button>";
					break;
				case 3://Servicios
					sGrupo3 += "<button class='btn btn-sm btn-Opc' onclick='ejecutarOpcion(" + index + ")'>" + sNombre + "</button>";
					break;
				default://Otros
					sGrupo4 += "<button class='btn btn-sm btn-Opc' onclick='ejecutarOpcion(" + index + ")'>" + sNombre + "</button>";
					break;
			}
		}
	});
	//Opciones en el Menu
	$("#divBodPag").html(sGrupo1);
	$("#divBodSol").html(sGrupo2);
	$("#divBodSer").html(sGrupo3);
	$("#divBodOtr").html(sGrupo4);
	mdlDesbloquear();
	//Validar si hay mensaje en revalidacion y se debe de ejeuctar algun pagina
	if(arrAccion.irespuesta > 0){
		var idOpcionReval 	= 0;
		var sBoton 			= "ACEPTAR";
		var sMensaje 		= arrAccion.cmensaje;
		var sFuncion 		= "mdlDesbloquearMsj();";

		if (arrAccion.accion == "revale1" || arrAccion.accion == "restringuir_revale1"){//Detonar Digitlizador de E1 Revalidacion
			idOpcionReval = 97;
		}
		
		if (arrAccion.accion == "mapareval"){//Detonar Mapa de Revalidacion
			idOpcionReval = 96;
		}

		if(idOpcionReval > 0){
			$.each(arrOpciones , function(index, val) {
				if(val.idopcion == idOpcionReval){
					sFuncion = "distribuirOpcionMenu(" + index + ");";
				}
			});
		}
		//Mostrar mensaje de emergente para revalidacion
		mdlMsjFunc(cTituloModal,sMensaje,sBoton,sFuncion);
	}
}

function validarTiempoMonitor(){
	var aResp = new Array();
	var bResp = true;
	$.ajax({
        async: false,
        cache: false,
        url: ligaCase,
        type: "POST",
        dataType: "JSON",
        data: { opcion: 3, idcons: "CONS07", arrdatos: iEmpleado},
        success: function (data) {
            if(data.irespuesta == 4){
				aResp = data["CONS07"][0];
				if(parseInt(aResp.irespuesta) > 0){
					bResp = false;
					iTiempoMonitor = parseInt(aResp.irespuesta);
				}
            }
        }, error: function (a, b, c) {
            alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
        }
	});
	return bResp;
}
//#endregion

/**************************************************************************************************************************************
 * <DETONAR OPCIONES DEL MENU>
**************************************************************************************************************************************/
//#region 
function ejecutarOpcion(opc){
	mdlDesbloquearMsj();
	if (parseInt(arrOpcionesMenu[opc].tipoaplicacion) == 2){//Generar submenu
		generacionSubmenu(opc);
	}else{//ejecutar Opcion
		distribuirOpcionMenu(opc);
	}
}

function generacionSubmenu(opc){
	var idPadre = arrOpcionesMenu[opc].idopcion;
	var cModal 	= arrOpcionesMenu[opc].descripcion;
	var sOpcion	= "";
	$.each(arrOpcionesMenu , function(index, val) {
		if(val.control == idPadre){//Validar botones del padres
			sOpcion += "<button class='btn btn-sm btn-Opc' onclick='ejecutarOpcion(" + index + ")'>" + correcionCaracteres(val.descripcion) + "</button>";
		}
	});
	mdlMsj(cModal,sOpcion);
}

function distribuirOpcionMenu(opc){
	var iAplicativo = parseInt(arrOpcionesMenu[opc].tipoaplicacion);
	switch (iAplicativo) {
		case 1://PAGINAS DE INTERNET
		case 5://PAGINA WEB CAPTURAS / SERVICIOS
		case 6://PAGINA WEB
		case 7://PAGINA WEB QUE REQUIERE NUMERO DE EMPLEADO
		case 9://PAGINA WEB QUE REQUIERE NUMERO DE EMPLEADO,TIENDA
		case 10://PAGINA WEB CAPTURAS / SERVICIOS (SIN CLUSTER)
		case 11://PAGINA WEB (SIN CLUSTER)
		case 12://PAGINA WEB QUE REQUIERE NUMERO DE EMPLEADO (SIN CLUSTER)
		case 13://PAGINA INTERNET (IE)
		case 14://PAGINA WEB NAVIUS(SIN CLUSTER)
		case 15://PAGINA WEB NAVIUS QUE REQUIERE NUMERO DE EMPLEADO (SIN CLUSTER)
			ejecutarNavegador(opc);
		break;
		case 3://EJECUTAR APLICACION
		case 4://EJECUTAR APLICACION AGREGANDO PROMOTOR COMO PARAMETRO
			ejecutarAplicacion(opc);
		break;
		case 8: //EXPLORARDOR SAFTV
			ejecutarSAFTV(opc);
		break;
		default:
			mdlMsj(cTituloModal,"Promotor: aplicacion no valida dentro del menu, favor de contactar a Mesa de ayuda.");
		break;
	}
}

function ejecutarNavegador(opc){
	var iOpcionMenu		= parseInt(arrOpcionesMenu[opc].idopcion);
	var bRespMonitor 	= true;
	var iAplicativo 	= parseInt(arrOpcionesMenu[opc].tipoaplicacion);
	var sUrl 			= "-new-window " + arrOpcionesMenu[opc].liga.trim();
	var iNavegador 		= parseInt(arrOpcionesMenu[opc].tiponavegador);

	if(iOpcionMenu == 98 || iOpcionMenu == 102){
		bRespMonitor = validarTiempoMonitor();
	}

	if(bRespMonitor){
		switch (iAplicativo) {
			case 5: case 10:
				sUrl += "?empleado=" + iEmpleado + "&password=" + sContrasenia;
			break;
			case 7: case 12: case 15: 
				sUrl += iEmpleado;
			break;
			case 9:
				sUrl += iEmpleado + "&tienda=" + iTienda;
			break;
		}
	
		switch (iNavegador) {
			case 0: 	iNavegador = NAVEGADOR_FIREFOX; break;
			case 1: 	iNavegador = NAVEGADOR_HTMLLOGIN; break;
			default: 	iNavegador = NAVEGADOR_NAVIUS; break;
		}
		//Detonar pagina
		opcionEjecuta(iNavegador,sUrl);
	}else{
		mdlMsj(cTituloModal,"Promotor, la consulta de tu monitor solo la  puedes realizar cada 30 minutos, quedan " + iTiempoMonitor + " minutos restantes para que puedas realizar la consulta nuevamente, favor  de seguir esperando.");
	}

}

function ejecutarAplicacion(opc){
	var iAplicativo = parseInt(arrOpcionesMenu[opc].tipoaplicacion);
	var sUrl 		= arrOpcionesMenu[opc].liga.trim();
	var sParametro 	= "";
	var iLigaexe	= sUrl.length;
	var iCorteliga 	= 0;

	switch (iAplicativo) {
		case 3: case 4:
			//se valida que se encuentre un espacio en blanco en caso de no tomar completamente la liga de bd
			if (sUrl.indexOf(" ") == -1){iCorteliga = iLigaexe;}
			else{iCorteliga = sUrl.indexOf(" ");}			
			sUrl		= sUrl.substring(0, iCorteliga);
			sParametro 	= sUrl.substring(iCorteliga + 1, iLigaexe);
			if(iAplicativo == 4){sParametro += " " + iEmpleado;}
			opcionEjecuta(sUrl,sParametro);
		break;
	}
}

function ejecutarSAFTV(opc){
	var sParametro 	= "";
	var sUrl 		= arrOpcionesMenu[opc].liga;
	if (sPasssaftv != "") {
		sParametro = sClaveconsar + " " + sPasssaftv;
		opcionEjecuta(sUrl,sParametro);
	}else {
		mdlMsj("Usuario sin contraseña para SAFTV. Pongase en contacto con el administrador del sistema.");
	}
}
//#endregion