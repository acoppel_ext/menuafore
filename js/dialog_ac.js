var iEmpleado					= getQueryVariable("empleado");
var cNombres 					= "";
var cApePaterno 				= "";
var cApeMaterno 				= "";
var cCurp	 					= "";
var cNss						= "";
var cCveConsar 					= "";
var cFecharegistro				= "";
var iTienda						= "";

function dialog_ac(divDialogo) {
    var divDlgMain = divDialogo;
    var iAltoDef = 200;
    var iAnchoDef = 250;
    var iAltoDlg = 0;
    var iAnchoDlg = 0;
    this.crear = function(iAncho, iAlto, sTitulo) {
        //divDlgMain.innerHTML="";
        if (iAlto == 'undefined' || iAlto == 0)
            iAltoDlg = iAltoDef;
        else
            iAltoDlg = iAlto;
        if (iAncho == 'undefined' || iAncho == 0)
            iAnchoDlg = iAnchoDef;
        else
            iAnchoDlg = iAncho;

        if (sTitulo == 'undefined') sTitulo = '';
        var sHtml = '<div id=\"divDialog\"><table border=\"0\" width=\"' + iAnchoDlg + '\" height=\"' + iAltoDlg + '\"">' +
            '<tr>' +
            '<td class=\"tituloDialogo\">' + sTitulo + '</td>' +
            '<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>' +
            '</tr>' +
            '<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>' +
            '<tr>' +
            '<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\"></div></td>' +
            '</tr>' +
            '<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>' +
            '<tr><td colspan=\"2\" height=\"5%\"></td></tr>' +
            '<tr><td align="center" colSpan="4">' +
            '<input type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"btndlg\" />' +
            '</td></tr>' +
            '</table></div>';
        divDlgMain.innerHTML = sHtml;
        document.getElementById('btnCerrarDlgx').onclick = function() { cerrar(); };
        document.getElementById('btnModalAcep').onclick = function() { cerrar(); };
        CapturadorDeEventos();
    };

    function CapturadorDeEventos() {
        if (window.addEventListener) {
            window.addEventListener("keydown", compruebaTecla, false);
        } else if (document.attachEvent) {
            document.attachEvent("onkeydown", compruebaTecla);
        }
    }

    function compruebaTecla(evt) {
        var tecla = evt.which || evt.keyCode;
        if (tecla == 27) {
            if (evt != null) {
                evt.which = 0;
                evt.preventDefault();
                evt.stopPropagation();
                evt.keyCode = 0;
                evt.returnValue = false;
                evt.cancelBubble = true;
            } else {
                event.keyCode = 0;
                event.returnValue = false;
                event.cancelBubble = true;
            }
            cerrar();
        }
    }
    this.mostrar = function(sContenido) {
        document.getElementById('divDialog').style.width = iAnchoDlg + 'px';
        document.getElementById('divDialog').style.height = iAltoDlg + 'px';
        divContentDlg = document.getElementById('divContentDlg');
        divContentDlg.innerHTML = sContenido;
        document.getElementById('divDialog').style.marginTop = ((document.body.clientHeight / 2) - (iAltoDlg / 2)) + 'px';
        divDlgMain.style.display = 'block';
    };

    function cerrar() {
        divDlgMain.innerHTML = "";
        divDlgMain.style.display = 'none';
    };
    this.agregarMensaje = function(sContenido) {
        divContentDlg = document.getElementById('divContentDlg');
        divContentDlg.innerHTML = divContentDlg.innerHTML + sContenido;
    };
    this.mostrarDialogo = function() {
        document.getElementById('divDialog').style.width = iAnchoDlg + 'px';
        document.getElementById('divDialog').style.height = iAltoDlg + 'px';
        document.getElementById('divDialog').style.marginTop = ((document.body.clientHeight / 2) - (iAltoDlg / 2)) + 'px';
        divDlgMain.style.display = 'block';
    };
    this.mensajeSesion = function(iAncho, iAlto, sTitulo) {
        divDlgMain.innerHTML = "";
        if (iAlto == 'undefined' || iAlto == 0)
            iAltoDlg = iAltoDef;
        else
            iAltoDlg = iAlto;
        if (iAncho == 'undefined' || iAncho == 0)
            iAnchoDlg = iAnchoDef;
        else
            iAnchoDlg = iAncho;

        if (sTitulo == 'undefined') sTitulo = '';
        var sHtml = '<div id=\"divDialog\"><table border=\"0\" width=\"' + iAnchoDlg + '\" height=\"' + iAltoDlg + '\"">' +
            '<tr>' +
            '<td class=\"tituloDialogo\">' + sTitulo + '</td>' +
            '<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>' +
            '</tr>' +
            '<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>' +
            '<tr>' +
            '<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" >' +
            '</div></td>' +
            '</tr>' +
            '<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>' +
            '<tr><td colspan=\"2\" height=\"5%\"></td></tr>' +
            '<tr id="botones1"><td align=\"center\" colSpan=\"4\">' +
            '<input  type=\"button\" id=\"btnCancelar\" value=\"NO\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
            '&nbsp;&nbsp;&nbsp;<input  type=\"button\" id=\"btnModalAcepC\" value=\"SI\" class=\"BtnModal\" />' +
            '</td></tr>' +
            '</table></div>';
        divDlgMain.innerHTML = sHtml;
        document.getElementById('btnCerrarDlgx').onclick = function() { cerrarDialogo(); };
        document.getElementById('btnCancelar').onclick = function() { cerrarDialogo(); };
        document.getElementById('btnModalAcepC').onclick = function() { cerrarSesiones(); };
    };

    function cerrarSesiones() {
        cerrarSesion(iEmpleado);
        generarToken(iEmpleado)

        if (!authorization) {
            mensaje(TIPO_MSJ_AVISO, "Sin conexión a servidor");
        } else {
            iIdPagina = PAG_MENU_BOTONES;
            cargarPagina();
            //EJECUCION PARA REPRODUCCION DEL VIDEO AL INICIAR EL MENUAFORE
            if (validarReproduccionVideo()) {
                Concurrent.Thread.create(function() {
                descargaVideo();
                mdlVideo(cBase64);
                document.getElementById("viDecalogo").play();
                document.getElementById("viDecalogo").onended = function() {
                    document.getElementById("divDlgVideo").innerHTML = "";
                    document.getElementById("divDlgVideo").style.display = 'none';
                    //funcion para actualizar registro de que ya vio el video
                    mostrarEjericios(iEmpleado);
                    actualizarReproduccionVideo();
                };
            });
            }else {
                mostrarEjericios(iEmpleado);
            }
            inicializarDivMenuBotones(iEmpleado);
        }
        divDlgMain.innerHTML = "";
        divDlgMain.style.display = 'none';
    }

    function cerrarDialogo() {
        divDlgMain.innerHTML = "";
        divDlgMain.style.display = 'none';
        document.getElementById('txtEmpleado').value = '';
        iEmpleado = 0;
        iOpcion = 0;
        arrMenuOpciones = [];
        arrEmpleado = [];
        cargarConfiguracion();
        iIdPagina = PAG_LOGIN_MENU;
        cargarPagina();
    }
}

function mensajeRevalidacion(divDialogoMensaje, noMensaje) {
    var divDlgMen = divDialogoMensaje;
    var iAltoDef = 200;
    var iAnchoDef = 250;
    var iAltoDlg = 0;
    var iAnchoDlg = 0;

    this.crear = function(iAncho, iAlto, sTitulo, indice) {
        //divDlgMain.innerHTML="";
        if (iAlto == 'undefined' || iAlto == 0)
            iAltoDlg = iAltoDef;
        else
            iAltoDlg = iAlto;
        if (iAncho == 'undefined' || iAncho == 0)
            iAnchoDlg = iAnchoDef;
        else
            iAnchoDlg = iAncho;

        if (sTitulo == 'undefined') sTitulo = '';
        var sHtml = '<div id=\"divDialogMen\"><table border=\"0\" width=\"' + iAnchoDlg + '\" height=\"' + iAltoDlg + '\"">' +
            '<tr>' +
            '<td class=\"tituloDialogo\" colspan=2 >' + sTitulo + '</td>' +
            '<td></td>' +
            '</tr>' +
            '<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>' +
            '<tr>' +
            '<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlgMens\"></div></td>' +
            '</tr>' +
            '<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>' +
            '<tr><td colspan=\"2\" height=\"5%\"></td></tr>' +
            '<tr><td align="center" colSpan="4">' +
            '<input type=\"button\" id=\"btnModalAcepMen\" value=\"Aceptar\" class=\"btndlg\" />' +
            '</td></tr>' +
            '</table></div>';
        divDlgMen.innerHTML = sHtml;

        document.getElementById('btnModalAcepMen').onclick = function() {
            switch (noMensaje) {
                case 1:
                    cerrar2();
                    break;
                case 2:
                    cerrar2();
                    ejecutarBoton(indice);
                    break;
            }
        };
    };

    function cerrar2() {
        divDlgMen.innerHTML = "";
        divDlgMen.style.display = 'none';
    };

    function cerrar() {
        divDlg.innerHTML = "";
        divDlg.style.display = 'none';
    };

    this.mostrar = function(sContenido) {
        document.getElementById('divDialogMen').style.width = iAnchoDlg + 'px';
        document.getElementById('divDialogMen').style.height = iAltoDlg + 'px';
        divContentDlg = document.getElementById('divContentDlgMens');
        divContentDlg.innerHTML = sContenido;
        document.getElementById('divDialogMen').style.marginTop = ((document.body.clientHeight / 2) - (iAltoDlg / 2)) + 'px';
        divDlgMen.style.display = 'block';
    };
    this.agregarMensaje = function(sContenido) {
        divContentDlg = document.getElementById('divContentDlgMens');
        divContentDlg.innerHTML = divContentDlg.innerHTML + sContenido;
    };
    this.mostrarDialogo = function() {
        document.getElementById('divDialogMen').style.width = iAnchoDlg + 'px';
        document.getElementById('divDialogMen').style.height = iAltoDlg + 'px';
        document.getElementById('divDialogMen').style.marginTop = ((document.body.clientHeight / 2) - (iAltoDlg / 2)) + 'px';
        divDlgMen.style.display = 'block';
    };
}

function dialog_ac2(divDialogo) {
    var divDlgMain = divDialogo;
    var iAltoDef = 200;
    var iAnchoDef = 250;
    var iAltoDlg = 0;
    var iAnchoDlg = 0;
    this.crear = function(iAncho, iAlto, sTitulo) {
        //divDlgMain.innerHTML="";
        if (iAlto == 'undefined' || iAlto == 0)
            iAltoDlg = iAltoDef;
        else
            iAltoDlg = iAlto;
        if (iAncho == 'undefined' || iAncho == 0)
            iAnchoDlg = iAnchoDef;
        else
            iAnchoDlg = iAncho;

        if (sTitulo == 'undefined') sTitulo = '';
        var sHtml = '<div id=\"divDialog2\"><table border=\"0\" width=\"' + iAnchoDlg + '\" height=\"' + iAltoDlg + '\"">' +
            '<tr>' +
            '<td class=\"tituloDialogo\">' + sTitulo + '</td>' +
            '<td><button id=\"btnCerrarDlgx2\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>' +
            '</tr>' +
            '<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>' +
            '<tr>' +
            '<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg2\"></div></td>' +
            '</tr>' +
            '<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>' +
            '<tr><td colspan=\"2\" height=\"5%\"></td></tr>' +
            '<tr><td align="center" colSpan="4">' +
            '<input type=\"button\" id=\"btnModalAcep2\" value=\"Aceptar\" class=\"btndlg\" />' +
            '</td></tr>' +
            '</table></div>';
        divDlgMain.innerHTML = sHtml;
        document.getElementById('btnCerrarDlgx2').onclick = function() { cerrar(); };
        document.getElementById('btnModalAcep2').onclick = function() { cerrar(); };
        CapturadorDeEventos();
    };

    function CapturadorDeEventos() {
        if (window.addEventListener) {
            window.addEventListener("keydown", compruebaTecla, false);
        } else if (document.attachEvent) {
            document.attachEvent("onkeydown", compruebaTecla);
        }
    }

    function compruebaTecla(evt) {
        var tecla = evt.which || evt.keyCode;
        if (tecla == 27) {
            if (evt != null) {
                evt.which = 0;
                evt.preventDefault();
                evt.stopPropagation();
                evt.keyCode = 0;
                evt.returnValue = false;
                evt.cancelBubble = true;
            } else {
                event.keyCode = 0;
                event.returnValue = false;
                event.cancelBubble = true;
            }
            cerrar();
        }
    }
    this.mostrar = function(sContenido) {
        document.getElementById('divDialog2').style.width = iAnchoDlg + 'px';
        document.getElementById('divDialog2').style.height = iAltoDlg + 'px';
        divContentDlg = document.getElementById('divContentDlg2');
        divContentDlg.innerHTML = sContenido;
        document.getElementById('divDialog2').style.marginTop = ((document.body.clientHeight / 2) - (iAltoDlg / 2)) + 'px';
        divDlgMain.style.display = 'block';
    };

    function cerrar() {
        divDlgMain.innerHTML = "";
        divDlgMain.style.display = 'none';
    };
    this.agregarMensaje = function(sContenido) {
        divContentDlg = document.getElementById('divContentDlg2');
        divContentDlg.innerHTML = divContentDlg.innerHTML + sContenido;
    };
    this.mostrarDialogo = function() {
        document.getElementById('divDialog2').style.width = iAnchoDlg + 'px';
        document.getElementById('divDialog2').style.height = iAltoDlg + 'px';
        document.getElementById('divDialog2').style.marginTop = ((document.body.clientHeight / 2) - (iAltoDlg / 2)) + 'px';
        divDlgMain.style.display = 'block';
    };
}

function dialog_acVideo(divDialogo) {
    var divDlgMain = divDialogo;
    this.crear = function(sTitulo) {

        if (sTitulo == 'undefined') sTitulo = '';
        var sHtml =
            '<div id=\"divDialogVideo\">' +
            '<video width="100%" height="100%" id="viDecalogo" >' +
            '<source src = "' + sTitulo + '" type = "video/webm" ></source>' +
            '</video >' +
            '</div>';
        divDlgMain.innerHTML = sHtml;

        document.getElementById('divDialogVideo').style.width = '80%';
        document.getElementById('divDialogVideo').style.height = '80%';
        document.getElementById('divDialogVideo').style.marginTop = ('5%');
        divDlgMain.style.display = 'block';
    };
}

function dialog_espere(divDialogo) {
    var divDlgMain = divDialogo;
    this.crear = function(sTitulo) {

        if (sTitulo == 'undefined') sTitulo = '';
        var sHtml =
            '<div id=\"divDialogVideo\">' +
            '<p>Espere ...</p>'+
            '</div>';
        divDlgMain.innerHTML = sHtml;

        document.getElementById('divDialogVideo').style.width = '80%';
        document.getElementById('divDialogVideo').style.height = '80%';
        document.getElementById('divDialogVideo').style.marginTop = ('5%');
        divDlgMain.style.display = 'block';
    };
}

function dialog_Html(divDialogo) {
	var divDlgMain = divDialogo;
	this.crear = function(sUrl) {
		var sHtml =
            '<div id="divDialogHtml" class="modal-dialog modalIframe" align="right"> ' +
            '       <div class="modal-body">' +
            '           <iframe src="' + sUrl + '" class="embed-responsive-item iframe-mdl" color: #fff; style="width:50rem;height:38rem;" allowfullscreen></iframe>' +
			'          	<button id="btnCerrar" style="margin-top:1rem;align:right;display:none;" type="button" class="btn btn-primary" onClick="cerrarVentana()">Cerrar</button>' +
            '       </div>' +
            '    </div>' +
            '</div>';
		divDlgMain.innerHTML = sHtml;

		document.getElementById('divDialogHtml').style.width = '810px';
		document.getElementById('divDialogHtml').style.height = '658px';
		document.getElementById('divDialogHtml').style.marginTop = ('5%');
		divDlgMain.style.display = 'block';
	};
}

function dialog_acuse(divDialogo) {
	var divDlgMain = divDialogo;
	var iAltoDef = 200;
	var iAnchoDef = 250;
	var iAltoDlg = 0;
	var iAnchoDlg = 0;
	this.crear = function (iAncho, iAlto, sTitulo) {
		//divDlgMain.innerHTML="";
		if (iAlto == 'undefined' || iAlto == 0)
			iAltoDlg = iAltoDef;
		else
			iAltoDlg = iAlto;
		if (iAncho == 'undefined' || iAncho == 0)
			iAnchoDlg = iAnchoDef;
		else
			iAnchoDlg = iAncho;

		if (sTitulo == 'undefined') sTitulo = '';
		var sHtml = '<div id=\"divDialog\"><table border=\"0\" width=\"' + iAnchoDlg + '\" height=\"' + iAltoDlg + '\"">' +
			'<tr>' +
			'<td class=\"tituloDialogo\">' + sTitulo + '</td>' +
			'</tr>' +
			'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>' +
			'<tr>' +
			'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\"></div></td>' +
			'</tr>' +
			'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>' +
			'<tr><td colspan=\"2\" height=\"5%\"></td></tr>' +
			'<tr><td align="center" colSpan="4">' +
			'<input type=\"button\"  id=\"btnModalAcep\" value=\"Acepto\" />' +
			'</td></tr>' +
			'</table></div>';
		divDlgMain.innerHTML = sHtml;
		document.getElementById('btnModalAcep').onclick = function () { cerrar(); };
		CapturadorDeEventos();
	};
	function CapturadorDeEventos() {
		if (window.addEventListener) {
			window.addEventListener("keydown", compruebaTecla, false);
		} else if (document.attachEvent) {
			document.attachEvent("onkeydown", compruebaTecla);
		}
	}
	function compruebaTecla(event) {
		event.preventDefault();
	}
	this.mostrar = function (sContenido) {
		document.getElementById('divDialog').style.width = iAnchoDlg + 'px';
		document.getElementById('divDialog').style.height = iAltoDlg + 'px';
		divContentDlg = document.getElementById('divContentDlg');
		divContentDlg.innerHTML = sContenido;
		document.getElementById('divDialog').style.marginTop = ((document.body.clientHeight / 2) - (iAltoDlg / 2)) + 'px';
		divDlgMain.style.display = 'block';
	};
	function cerrar() {
		divDlgMain.innerHTML = "";
		divDlgMain.style.display = 'none';
		//se obtienen los datos del promotor
		$.ajax
		({
			async		: false,
			cache		: false,
			url			: "php/COpcionesAcuse.php",
			type		: 'POST',
			dataType	: 'JSON',
			data		: {opcion:4, empleado:iEmpleado},
			success: function(data)
			{
				cNombres 				= data.nombres;
				cApePaterno 			= data.apepaterno;
				cApeMaterno 			= data.apematerno;
				cCurp	 				= data.curp;
				cNss					= data.nss;
				cCveConsar				= data.cveconsar;
				cFecharegistro			= data.fecharegistro;
				iTienda				 	= data.tienda;
				/*alert("nombres: "+cNombres+"\napepat: "+cApePaterno+"\napemat: "+cApeMaterno+"\ncurp: "+cCurp
				+"\nnss: "+cNss+"\ncveconsar: "+cCveConsar+"\nfechareg: "+cFecharegistro+"\ntienda: "+iTienda);*/
			}
		});
		//se hace el insert del acuse
		$.ajax
		({
			async		: false,
			cache		: false,
			url			: "php/COpcionesAcuse.php",
			type		: 'POST',
			dataType	: 'JSON',
			data		: {
				opcion:3,
				empleado:iEmpleado,
				nss:cNss,
				cveconsar:cCveConsar,
				nombres:cNombres,
				apepat:cApePaterno,
				apemat:cApeMaterno,
				curp:cCurp,
				fecharegistro:cFecharegistro,
				tienda:iTienda
			},
			success: function(data)
			{
				//se genera el formato con los datos del promotor
				$.ajax
				({
					async		: false,
					cache		: false,
					url			: "../generaracuseetica/generarAcuseEtica.php",
					type		: 'POST',
					dataType	: 'JSON',
					data		: 	{	iEmpleado:iEmpleado	},
					success: function(data)
					{
							
					}
				});					
			}
		});
	};
	this.agregarMensaje = function (sContenido) {
		divContentDlg = document.getElementById('divContentDlg');
		divContentDlg.innerHTML = divContentDlg.innerHTML + sContenido;
	};
	this.mostrarDialogo = function () {
		document.getElementById('divDialog').style.width = iAnchoDlg + 'px';
		document.getElementById('divDialog').style.height = iAltoDlg + 'px';
		document.getElementById('divDialog').style.marginTop = ((document.body.clientHeight / 2) - (iAltoDlg / 2)) + 'px';
		divDlgMain.style.display = 'block';
	};

}
function getQueryVariable(varGet)
{	
	//Obtener la url compleata del navegador
	var sUrl = window.location.search.substring(1);
	//Separar los parametros junto con su valor
	var vars = sUrl.split("&");
	for (var i=0;i<vars.length;i++) 
	{
		//Obtener el parametro y su valor en arreglo de 2
		var par = vars[i].split("=");
		if(par[0] == varGet) // [0] nombre de variable, [1] valor
			return par[1];
	}
	return(false);
}