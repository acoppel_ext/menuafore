function inicializarDivLoginMenu()
{
	document.getElementById("btnEntrar").onclick=function()
	{ 
		if(document.getElementById("txtNumEmpleado").value !='')
		{
			iOpcion=IDENTIFICAR_EMP;
			document.getElementById("btnEntrar").disabled = true;
			cargarHuella();
		}
		else
			mensaje(TIPO_MSJ_AVISO, "<b>Favor de capturar número de empleado</b>");
	}
	document.getElementById("btnApagar").onclick=function()
	{ 
		if(confirm('¿ Confirmar APAGADO de computadora ?')==true)
		{
			for(i=0; i<arrAplicaciones.length;i++)
			{
				if(arrAplicaciones[i].nombre=='apagarPc')
				{
					ejecutaWebService(arrAplicaciones[i].valor, '');

					/*var sHtml=
						"<APPLET CODE  = \"appafore.class\" " +
						"ARCHIVE  = \"../applet/appafore.jar\" " +
						"ID = \"appafore\" " +
						"WIDTH    = 1 " +
						"HEIGHT   = 1 > " +
						"<param name=\"opcion\" value=\"3\"> " +
						"<param name=\"comando\" value=\""+ arrAplicaciones[i].valor + "\">" +
						"<param name=\"argumentos\" value=\"\"> " +
						"</APPLET>";
					document.getElementById('divApplet').innerHTML=sHtml;*/
					i=arrAplicaciones.length;
				}
			}
		}
	}
	document.getElementById('txtNumEmpleado').onkeypress=function(evento)
	{
		var charCode = (evento.which) ? evento.which : evento.keyCode
		if(esTeclaNumerica(evento))
		{
			if(charCode==13)
			{
				var btnLogin = document.getElementById("btnEntrar");
				if(btnLogin)
					btnLogin.click();
			}
			return true;
		}
		return false;
	}
}
function cargarHuella()
{
	var i=0; var bFlag=false; var sApp=""; var sIpHlla="";
	for(i=0; i<arrAplicaciones.length;i++)
	{
		if(arrAplicaciones[i].nombre=='validarEmpleado')
		{
			sApp=arrAplicaciones[i].valor;
			bFlag=true;
			i=arrAplicaciones.length;
		}
	}
	if(bFlag)
	{
		for(i=0; i<arrServidores.length;i++)
		{
			if(arrServidores[i].nombre=='he')
			{
				sIpHlla=arrServidores[i].valor;
				bFlag=true;
				i=arrServidores.length;
			}
		}
		if(bFlag)
		{
			ejecutaWebService(sApp, sIpHlla);

			/*sHtml=
				"<APPLET CODE  = \"appafore.class\" " +
				"ARCHIVE  = \"../applet/appafore.jar\" " +
				"ID = \"appafore\" " +
				"WIDTH    = 1 " +
				"HEIGHT   = 1 > " +
				"<param name=\"opcion\" value=\"3\"> " +
				"<param name=\"comando\" value="+ sApp + ">" +
				"<param name=\"argumentos\" value=" + sIpHlla + "> " +
				"</APPLET>";
			document.getElementById('divApplet').innerHTML=sHtml;*/
		}
	}
}
function esTeclaNumerica(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    {   
    	if(charCode==13)
    		return true;
    	else		
        	return false;
    }
    return true;
}
