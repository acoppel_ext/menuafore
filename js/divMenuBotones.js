var iOpcionMsj = 0;
var bMostrarModalAcuse			= true;
var iEmpleado					= getQueryVariable("empleado");
var cNss						= "";
var cCveConsar 					= "";

function inicializarDivMenuBotones(iEmpleado) {
	
	ValidacionesModalAcuse();
	if(bMostrarModalAcuse)
	{
		mensajeAcuseEtica('Recibí de AFORE COPPEL S.A de C.V., empresa a la que presto mis  servicios, el presente "Código de Ética" vigente '+
		'a partir del 25 de  noviembre de 2019 y manifiesto bajo protesta de decir verdad ante la   Administradora, que he leído, conozco '+
		'y me comprometo a aceptar y  adoptar sus principios, así como también a difundir y asegurar la   compresión de su contenido entre mis '+
		'compañeros de trabajo, con el fin  de preservar y enaltecer la integridad de la empresa.');
	}
	
	document.getElementById("btnHistoMensajes").onclick = function () {
		iOpcionMsj = 2;
		consultarMensajes();
	}
	consultarOpciones(iEmpleado);
	iOpcionMsj = 1;
	consultarMensajes();
	document.getElementById('divBotones').style.height = ((document.body.clientHeight / 2)) + 'px';
	document.getElementById('divMensajes').style.height = ((document.body.clientHeight / 3) - 40) + 'px';
}

function consultarOpciones(iEmpleado) {
	var sUrl = arrPaginas[PAG_MNU_OPCIONES];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, { empleado: iEmpleado }, ajaxRespConsultarOpciones, false, 'POST');
}

function ajaxRespConsultarOpciones(sAjaxResp) {
	var datos = eval('(' + sAjaxResp + ')');
	arrMenuOpciones = datos.opciones;
	var indice = 0;
	var iopcionReval = 1;

	if (datos.estado != 0 && datos.estado != null && datos.estado != -7) {

		if (datos.metodo == 'sinaccion' || datos.metodo == 'revale1') {
			iOpcion = 1;
		}
		else if (datos.metodo == 'restringuir' || datos.metodo == 'restringuir_revale1') {
			iOpcion = 2;
		}
		else if (datos.metodo == 'postulante') {
			iOpcion = 1;
		}

		if (iOpcion == 2) {
			var arrMenuOpcionesReval = new Array();
			for (var i = 0; i < arrMenuOpciones.length; i++) {
				if (arrMenuOpciones[i].idopcion == 37 ||
					arrMenuOpciones[i].idopcion == 44 ||
					arrMenuOpciones[i].idopcion == 45 ||
					arrMenuOpciones[i].idopcion == 96 ||
					arrMenuOpciones[i].idopcion == 97) {
					arrMenuOpcionesReval.push
						({
							"control": arrMenuOpciones[i].control,
							"descripcion": arrMenuOpciones[i].descripcion,
							"grupo": arrMenuOpciones[i].grupo,
							"idopcion": arrMenuOpciones[i].idopcion,
							"liga": arrMenuOpciones[i].liga,
							"subcontrol": arrMenuOpciones[i].subcontrol,
							"tipoaplicacion": arrMenuOpciones[i].tipoaplicacion,
							"tiponavegador": arrMenuOpciones[i].tiponavegador
						});
				}
			}
			arrMenuOpciones = arrMenuOpcionesReval;
		}

		if (datos.metodo == 'revale1' || datos.metodo == 'restringuir_revale1') {
			for (var i = 0; i < arrMenuOpciones.length; i++) {
				if (arrMenuOpciones[i].idopcion == 97) //-- botones padres
				{
					indice = i;
					break;
				}
			}
		}
		else if (datos.metodo == 'mapareval') {
			for (var i = 0; i < arrMenuOpciones.length; i++) {
				if (arrMenuOpciones[i].idopcion == 96) //-- botones padres
				{
					indice = i;
					break;
				}
			}
		}

		if (indice > 0) { iopcionReval = 2; }
		mostraMensajeRevalidacion(datos.descripcion, iopcionReval, indice);
		// setTimeout(function(){
			// mostraMensajeRevalidacion(datos.descripcion, iopcionReval, indice);
		// },10000);
	}
	else {
		if (datos.estado == 0 || datos.estado == null) {
			iOpcion = 1;
		}
		else
			mensaje(TIPO_MSJ_ERROR, datos.descripcion);
	}

	var sBotones = '<table width=\"100%\" border=\"0\"><tr><td align=\"center\">';
	var sBtnLigas = "";
	var sBtnSolicitud = "";
	var sBtnServicio = "";
	var sBtnOtros = "";
	var sBoton = "";

	//Mostrar los Botones
	sBtnLigas = '<table border=\"0\" class=\"bordeRedondo\"><tr><td class=\"bordeRedondo titulo\">Páginas de Internet</td></tr>';
	sBtnSolicitud = '<table border=\"0\" class=\"bordeRedondo\"><tr><td class=\"bordeRedondo titulo\">Solicitudes</td></tr>';
	sBtnServicio = '<table border=\"0\" class=\"bordeRedondo\"><tr><td class=\"bordeRedondo titulo\">Servicios</td></tr>';
	sBtnOtros = '<table border=\"0\" class=\"bordeRedondo\"><tr><td class=\"bordeRedondo titulo\">Otros</td></tr>';
	for (var i = 0; i < arrMenuOpciones.length; i++) {
		if (arrMenuOpciones[i].control == 0) //-- botones padres
		{
			if (arrMenuOpciones[i].tipoaplicacion == '2')
				sBoton = '<tr><td><button class=\"btn btn-primary btnMenu\" id=\"btnOpcion' + arrMenuOpciones[i].idopcion + '\" onclick=\"generarSubmenu(' + i + ');\">' + arrMenuOpciones[i].descripcion + '</button></td></tr>';
			else
				sBoton = '<tr><td><button class=\"btn btn-primary btnMenu\" id=\"btnOpcion' + arrMenuOpciones[i].idopcion + '\" onclick=\"ejecutarBoton(' + i + ');\">' + arrMenuOpciones[i].descripcion + '</button></td></tr>';

			switch (arrMenuOpciones[i].grupo) {
				case '1': sBtnLigas += sBoton; break;
				case '2': sBtnSolicitud += sBoton; break;
				case '3': sBtnServicio += sBoton; break;
				default: sBtnOtros += sBoton; break;
			}
		}
	}
	sBtnLigas += '</table>';
	sBtnSolicitud += '</table>';
	sBtnServicio += '</table>';
	sBtnOtros += '</table>';
	sBotones += '<table border=\"0\"><tr valign=\"top\"><td>' + sBtnLigas + '</td><td>' + sBtnSolicitud + '</td><td>' + sBtnServicio + '</td><td>' + sBtnOtros + '</td></tr></table>';
	document.getElementById('divBotones').innerHTML = sBotones;

}

function ejecutarBoton(iPosArray) {
	switch (arrMenuOpciones[iPosArray].tipoaplicacion) {
		case '1'://PAGINAS DE INTERNET
		case '5'://PAGINA WEB CAPTURAS / SERVICIOS
		case '6'://PAGINA WEB
		case '7'://PAGINA WEB QUE REQUIERE NUMERO DE EMPLEADO
		case '9'://PAGINA WEB QUE REQUIERE NUMERO DE EMPLEADO,TIENDA
		case '10'://PAGINA WEB CAPTURAS / SERVICIOS (SIN CLUSTER)
		case '11'://PAGINA WEB (SIN CLUSTER)
		case '12'://PAGINA WEB QUE REQUIERE NUMERO DE EMPLEADO (SIN CLUSTER)
		case '13'://PAGINA INTERNET (IE)
		case '14'://PAGINA WEB NAVIUS(SIN CLUSTER)
		case '15'://PAGINA WEB NAVIUS QUE REQUIERE NUMERO DE EMPLEADO (SIN CLUSTER)
		case '16':
			if(arrMenuOpciones[iPosArray].idopcion == '98' || arrMenuOpciones[iPosArray].idopcion == '102'){
				if(validarTiempoMonitor()){
					ejecutarNavegador(iPosArray);
				}
			}
			else if(arrMenuOpciones[iPosArray].idopcion == '115')
			{ //Validacion para entrar al boton de obtener ip
				ejecutarNavegador(iPosArray);

			}else if(arrMenuOpciones[iPosArray].idopcion == '43'){ //Validacion para entrar al boton de supervisor
				
				validarAccesoSupervisores(iPosArray);

			} else if(arrMenuOpciones[iPosArray].idopcion == '42'){//Validacion para entrar al boton agente promotor
			
				validarAccesoPromotor(iPosArray);
			}
			else{
				ejecutarNavegador(iPosArray);
			}
			break;
		case '2':
			break;
		case '3'://EJECUTAR APLICACION
		case '4'://EJECUTAR APLICACION AGREGANDO PROMOTOR COMO PARAMETRO
			ejecutarAplicacion(iPosArray);
			break;
		case '8': //EXPLORARDOR SAFTV
			ejecutarSAFTV(iPosArray);
			break;
		default:
			break;
	}
}

function mostraMensajeRevalidacion(mensaje, numeroMen, indice) {
	var Titulo = "Revalidacion Necesaria.";
	var contenido = '<table border=\"0\" width=\"100%\"><tr><td>' + mensaje + '</td></tr></table>';
	var myDlg = new mensajeRevalidacion(document.getElementById('divMensaje'), numeroMen);
	myDlg.crear(500, 300, Titulo, indice);
	myDlg.mostrar(contenido);
}

function generarSubmenu(iPosArray) {
	var sBotones = "";
	var iIdPadre = arrMenuOpciones[iPosArray].idopcion;
	var sTitulo = arrMenuOpciones[iPosArray].descripcion;
	if (arrMenuOpciones[iPosArray].tipoaplicacion == '2' || arrMenuOpciones[iPosArray].tipoaplicacion == '7') {
		sBotones += '<table border=\"0\" width=\"100%\"><tr><td align=\"center\"><table>';
		for (var i = 0; i < arrMenuOpciones.length; i++) {
			if (arrMenuOpciones[i].control == iIdPadre) //-- botones hijos
				sBotones += '<tr><td><button class=\"btn btn-primary btnMenu\" id=\"btnOpcion' + arrMenuOpciones[i].idopcion + '\" onclick=\"ejecutarBoton(' + i + ');\">' + arrMenuOpciones[i].descripcion + '</button></td></tr>';
		}
		sBotones += '</table></td></tr></table>';
		var myDlg = new dialog_ac(document.getElementById('divSubMenu'));
		myDlg.crear(500, 600, sTitulo);
		myDlg.mostrar(sBotones);
	}
}

function consultarMensajes() {
	var sUrl = arrPaginas[PAG_MNU_MENSAJES];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, { opcion: iOpcionMsj }, ajaxRespConsultarMensajes, false, 'POST');
}

function ajaxRespConsultarMensajes(sAjaxResp) {
	var arrReg = {
		anchoTabla: '100%',
		altoTabla: 560,
		anchoCols: [100, 895, 100, 100],
		altoRenglon: 'auto',
		columnas: ["Fecha Alta", "Descripción", "Creador", "Activo"],
		nomColumnas: ["fechaalta", "descripcion", "empleado", "estatus"],
		mostrarColumna: [true, true, true, true],
		alinearColumna: ['centrado', 'justificado', 'centrado', 'centrado'],
		registros: []
	};
	var myDlg = new dialog_ac(document.getElementById('divHistoMensajes'));
	myDlg.crear(1150, 680, 'HISTÓRICO DE MENSAJES');
	myDlg.agregarMensaje('<table id="gridHistoMensajes" class="jsgrid"></table>');
	var datos = eval('(' + sAjaxResp + ')');
	if (datos.estado == 1) {
		if (iOpcionMsj == 1) {
			document.getElementById('txtMensajeActual').value = 'Mensaje publicado el ' + datos.mensajes[0].fechaalta;
			document.getElementById('divMensajes').innerHTML = datos.mensajes[0].descripcion;
		}
		else {
			for (var i = 0; i < datos.mensajes.length; i++) {
				arrReg.registros.push(new Array(datos.mensajes[i].fechaalta, datos.mensajes[i].descripcion, datos.mensajes[i].empleado, datos.mensajes[i].estatus));
			}
			var jsGridHistoMsj = new jsgrid(document.getElementById("gridHistoMensajes"));
			jsGridHistoMsj.agregarRegistro(arrReg);
			myDlg.mostrarDialogo();
		}
	}
	else {
		document.getElementById('txtMensajeActual').value = 'Mensaje';
		document.getElementById('divMensajes').innerHTML = datos.descripcion;
	}
}

function ejecutarAplicacion(iPosArray) {

	if (arrMenuOpciones[iPosArray].tipoaplicacion == '3' || arrMenuOpciones[iPosArray].tipoaplicacion == '4') {
		//Ruta del EXE
		var cligaexe = arrMenuOpciones[iPosArray].liga.trim();
		//Longuitud de la cadena con la liga del exe
		var iligaexe = cligaexe.length;
		//Buscar espacio en blanco de la ruta para saber donde se va a realizar el corte
		var icorteliga = 0;
		if (cligaexe.indexOf(' ') == -1)//se valida que se encuentre un espacio en blanco en caso de no tomar completamente la liga de bd
			icorteliga = iligaexe;
		else
			icorteliga = cligaexe.indexOf(' ');
		//Ruta del exe sin parametros asta donde encontro el espacio en blanco
		var cligasinparam = cligaexe.substring(0, icorteliga);
		//Parametros de la liga si es que trae posicion + 1 de donde se encontro el espacio y el limite es 
		var cparametrosliga = cligaexe.substring(icorteliga + 1, iligaexe);

		if (arrMenuOpciones[iPosArray].tipoaplicacion == '3')
			ejecutaWebService(cligasinparam, cparametrosliga);
		else if (arrMenuOpciones[iPosArray].tipoaplicacion == '4')
			ejecutaWebService(cligasinparam, cparametrosliga + " " + arrEmpleado[0].empleado);
	}
}

function ejecutarNavegador(iPosArray) {
	var sHtml = ""; var sUrl = "";
	if (arrMenuOpciones[iPosArray].tipoaplicacion == '5' || arrMenuOpciones[iPosArray].tipoaplicacion == '10')
		sUrl = arrMenuOpciones[iPosArray].liga + '?empleado=' + arrEmpleado[0].empleado + '&password=' + arrEmpleado[0].contrasenia;
	else if (arrMenuOpciones[iPosArray].tipoaplicacion == '6' || arrMenuOpciones[iPosArray].tipoaplicacion == '11' || arrMenuOpciones[iPosArray].tipoaplicacion == '14')
		sUrl = arrMenuOpciones[iPosArray].liga
	else if (arrMenuOpciones[iPosArray].tipoaplicacion == '7' || arrMenuOpciones[iPosArray].tipoaplicacion == '12' || arrMenuOpciones[iPosArray].tipoaplicacion == '15')
		sUrl = arrMenuOpciones[iPosArray].liga + arrEmpleado[0].empleado;
	else if (arrMenuOpciones[iPosArray].tipoaplicacion == '9')
		sUrl = arrMenuOpciones[iPosArray].liga + arrEmpleado[0].empleado + '&tienda=' + arrEmpleado[0].tienda;
	else if (arrMenuOpciones[iPosArray].tipoaplicacion == '16')
		sUrl = arrMenuOpciones[iPosArray].liga + arrEmpleado[0].empleado + '&iporigen=' + sIpLocal;
	else
		sUrl = arrMenuOpciones[iPosArray].liga;

	var sparametro = "-new-window " + sUrl;

	if (arrMenuOpciones[iPosArray].tiponavegador == '0')
		ejecutaWebService(sFirefox, sparametro);
	else if (arrMenuOpciones[iPosArray].tiponavegador == '1')
		ejecutaWebService(sHtmlLogin, sUrl);
	else
		ejecutaWebService(sNavius, sUrl);
}

function ejecutarSAFTV(iPosArray) {
	var sHtml = "";
	if (arrEmpleado[0].passsaftv != '') {
		var sparametro = arrEmpleado[0].claveconsar + ' ' + arrEmpleado[0].passsaftv;
		ejecutaWebService(arrMenuOpciones[iPosArray].liga, sparametro);
	}
	else
		mensaje(TIPO_MSJ_AVISO, 'Usuario sin contraseña para SAFTV. \\n Pongase en contacto con el administrador del sistema');
}

function mensajeAcuseEtica(miMensaje)
{
	var modalAcuse = new dialog_acuse(document.getElementById('divDlgMensaje'));
	modalAcuse.crear(600,270,'ACUSE CODIGO DE ÉTICA');
	modalAcuse.mostrar(miMensaje);
}

function ValidacionesModalAcuse() {
	LLenaDatosPromotor();
	ValidaAcuseGenerado();
	if(bMostrarModalAcuse)
	{
		ValidaCveConsar();
	}
	
}

function LLenaDatosPromotor()
{
	$.ajax
    ({
		async		: false,
		cache		: false,
		url			: "php/COpcionesAcuse.php",
		type		: 'POST',
		dataType	: 'JSON',
		data		: {opcion:4, empleado:iEmpleado},
		success: function(data)
		{
			cNss					= data.nss;
			cCveConsar				= data.cveconsar;
		}
	});
}

function ValidaCveConsar()
{
	$.ajax
	({
		async		: false,
		cache		: false,
		url			: "php/COpcionesAcuse.php",
		type		: 'POST',
		dataType	: 'JSON',
		data		: {opcion: 1, empleado: iEmpleado},
		success: function(data)
		{
			if(data.respuesta==0){
				bMostrarModalAcuse=false;
			}
		}
	});
}

function ValidaAcuseGenerado()
{
	$.ajax
		({
			async		: false,
			cache		: false,
			url			: "php/COpcionesAcuse.php",
			type		: 'POST',
			dataType	: 'JSON',
			data		: {opcion: 2, opcionAcuseGenerado:1, empleado: iEmpleado, cveconsar: cCveConsar, nss:cNss},
			success: function(data)
			{
				if(data.respuesta==0){
					bMostrarModalAcuse=false;
				}
			}
		});
}

function getQueryVariable(varGet)
{	
	//Obtener la url compleata del navegador
	var sUrl = window.location.search.substring(1);
	//Separar los parametros junto con su valor
	var vars = sUrl.split("&");
	for (var i=0;i<vars.length;i++) 
	{
		//Obtener el parametro y su valor en arreglo de 2
		var par = vars[i].split("=");
		if(par[0] == varGet) // [0] nombre de variable, [1] valor
			return par[1];
	}
	return(false);
}