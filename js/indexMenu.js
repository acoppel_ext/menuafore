var arrEjecAppInicio = Array();
var arrServidores = Array();
var arrAplicaciones = Array();
var arrEmpleado = Array();
var arrMenuOpciones = Array();
var iOpcion = 0;
var opcionSupervidores = 0;
var opcionPromotor = 0;
var iEmpleado = 0;
var divContenedor;
var sIpLocal = "";
var iIdPagina = 0;
var iEstado = 0;
var bTiempoMonitor = false;
var bReproduccionVideo = false;
var cBase64 = '';
var iKeyx = 0;
var idVideo = 0;

var authorization = '';

document.onreadystatechange = function() {
	document.onmousedown = anularBotonDerecho;

	if (document.readyState == 'complete') {
		divContenedor = document.getElementById("divContenedor");
		document.getElementById("imgCerrarSesion").onclick = function() {
			cerrarSesion(iEmpleado);
			document.getElementById('txtEmpleado').value = '';
			iEmpleado = 0;
			iOpcion = 0;
			arrMenuOpciones = [];
			arrEmpleado = [];
			cargarConfiguracion();
			iIdPagina = PAG_LOGIN_MENU;
			cargarPagina();
		}
		Concurrent.Thread.create(cargarConfiguracion);
		Concurrent.Thread.create(ejecutarAplicacionInicio);
		iIdPagina = PAG_LOGIN_MENU;
		Concurrent.Thread.create(cargarPagina);
	}
};

function anularBotonDerecho(e) {
	if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2)) {
		alert("Promotor por seguridad no puede usar esta opcion");
		return false;
	}
}

function cargarPagina() {
	var sUrl = arrPaginas[iIdPagina];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, {}, ajaxRespCargarPagina, false, 'POST');
}

function cargarConfiguracion() {
	var sUrl = arrPaginas[PAG_LEER_WEB_CONF];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, {}, ajaxRespConfiguracion, false, 'POST');
}

function ajaxRespConfiguracion(sAjaxResp) {
	var arrConf = eval('(' + sAjaxResp + ')');
	if (arrConf.estado == 1) {
		sIpLocal = arrConf.IpLocal;
		arrEjecAppInicio = arrConf.EjecutarAplicacionInicio;
		arrServidores = arrConf.Servidores;
		arrAplicaciones = arrConf.aplicaciones;
		document.getElementById('txtFecha').value = arrConf.fecha;
	} else
		mensaje(TIPO_MSJ_ERROR, "Error al leer configuración del sistema");
}

function ajaxRespCargarPagina(sAjaxResp) {
	divContenedor.innerHTML = "";
	divContenedor.innerHTML = sAjaxResp;
	switch (iIdPagina) {
		case PAG_LOGIN_MENU:
			inicializarDivLoginMenu();
			break;
		default:
			break;
	}
}

function validarTiempoMonitor() {
	var sUrl = arrPaginas[PAG_TIEMPO_MONITOR];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, { empleado: iEmpleado }, ajaxRespValidarTiempoMonitor, false, 'POST');
	return bTiempoMonitor;
}

function ajaxRespValidarTiempoMonitor(sAjaxResp) {
	bTiempoMonitor = false;
	var datos = eval('(' + sAjaxResp + ')');
	if (parseInt(datos.tiempoMonitor) == 0) {
		bTiempoMonitor = true;
	} else {
		var mensAfi = 'Promotor, la consulta de tu monitor solo la  puedes realizar cada 30 minutos, quedan ' + datos.tiempoMonitor + ' minutos restantes para que puedas realizar la consulta nuevamente, favor  de seguir esperando.';
		document.getElementById("divDlgMensaje2").innerHTML = "";
		mensajeTiempoMonitor(mensAfi);
	}

}

function validarReproduccionVideo() {
	var sUrl = arrPaginas[PAG_REPRODUCCION_VIDEO];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, { empleado: iEmpleado + '-1' }, ajaxRespValidarReproduccionVideo, false, 'POST');
	return bReproduccionVideo;
}

function ajaxRespValidarReproduccionVideo(sAjaxResp) {
	bReproduccionVideo = false;
	var datos = eval('(' + sAjaxResp + ')');
	if (parseInt(datos.respuesta) == 1) {
		bReproduccionVideo = true;
		idVideo = datos.id;
	}

}

function actualizarReproduccionVideo() {
	var sUrl = arrPaginas[PAG_REPRODUCCION_VIDEO];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, { empleado: iEmpleado + '-2' }, ajaxRespActualizarReproduccionVideo, false, 'POST');
}

function ajaxRespActualizarReproduccionVideo(sAjaxResp) {
	var datos = eval('(' + sAjaxResp + ')');
}

function descargaVideo() {
	var sUrl = arrPaginas[PAG_DESCARGA_VIDEO];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, { id: idVideo }, ajaxRespdescargaVideo, false, 'POST');
}

function ajaxRespdescargaVideo(sAjaxResp) {
	var datos = eval('(' + sAjaxResp + ')');
	cBase64 = datos.fndescargavideo;
}

function mdlVideo(videoCont) {
	var myDlg2 = new dialog_acVideo(document.getElementById('divDlgVideo'));
	myDlg2.crear(videoCont);
}

function espere() {
	var myDlg2 = new dialog_espere(document.getElementById('divDlgEspere'));
	myDlg2.crear('');
}

function mdlHtml(sUrl) {
	var myDlg2 = new dialog_Html(document.getElementById('myModalHtml'));
	myDlg2.crear(sUrl);

	setTimeout(function() {
		document.getElementById("btnCerrar").style.display = 'block';
	}, 60000);
}

function cerrarVentana() {
	actualizarEjercicio();
	$(".modal-content").css("display", "none");
	document.getElementById("myModalHtml").innerHTML = "";
	document.getElementById("myModalHtml").style.display = 'none';
	iConsecutivo = 0;
	setTimeout(function() {
		mostrarEjericios(iEmpleado);
	}, 1000);
}

function mensajeTiempoMonitor(sMensaje) {
	var myDlg2 = new dialog_ac2(document.getElementById('divDlgMensaje2'));
	myDlg2.crear(500, 300, 'MENU AFORE');
	myDlg2.mostrar(sMensaje);
}

function mensaje(iTipo, sMensaje) {
	var myDlg = new dialog_ac(document.getElementById('divDlgMensaje'));
	myDlg.crear(600, 400, 'MENU AFORE');
	myDlg.mostrar(sMensaje);
}

function consultarEmpleado(iEmpleado) {
	var sUrl = arrPaginas[PAG_CONSULTAR_EMPL];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, { empleado: iEmpleado }, ajaxRespConsultarEmpleado, false, 'POST');
}

function ajaxRespConsultarEmpleado(sAjaxResp) {
	var datos = eval('(' + sAjaxResp + ')');
	iEstado = 0;
	if (datos.estado == 1) {
		arrEmpleado = datos.datosempleado;
		document.getElementById('txtEmpleado').value = ' ' + arrEmpleado[0].nombre + ' ' + arrEmpleado[0].apellidopat + ' ' + arrEmpleado[0].apellidomat + ' - Centro:' +
			arrEmpleado[0].centro + ' ' + arrEmpleado[0].nombrecentro;
		iEstado = datos.estado;
	} else
		mensaje(TIPO_MSJ_ERROR, datos.descripcion);
}

function consultarEmpleadosup(numempleadosup) {
	var sUrl = arrPaginas[PAG_CONSULTAR_EMPLSUP];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, { numempleadosup: iEmpleado }, ajaxRespConsultarEmpleadosup, false, 'POST');


}
function ajaxRespConsultarEmpleadosup(sAjaxResp) {
	var mensaje = '';
	var obj = JSON.parse(sAjaxResp);

	console.log(obj);


	if (obj['empleadovalidadosup'] != 0) {
		document.getElementById("Modaltst").style.display = 'none';
		document.getElementById("txtNumEmpleado").value = "";
		ejecutarNavegador(opcionSupervidores);

	}
	else {
		mensajeTiempoMonitor("El número de empleado y huella No corresponden a un supervisor autorizado");
	}
}

function consultarPromotor(numempleadosup) {
	var sUrl = arrPaginas[PAG_CONSULTAR_PROMOTOR];
	var ajax = new ajax_ac();
	ajax.conectar();
	ajax.enviar(sUrl, { numempleadosup: iEmpleado }, ajaxRespConsultarEmpleadopromotor, false, 'POST');


}
function ajaxRespConsultarEmpleadopromotor(sAjaxResp) {
	var obj = JSON.parse(sAjaxResp);
	var mensaje = '';

	if (obj['empleadovalidadosup'] != 0) {
		document.getElementById("Modaltst").style.display = 'none';
		document.getElementById("txtNumEmpleado").value = "";
		ejecutarNavegador(opcionPromotor);
	}
	else {
		mensajeTiempoMonitor("El número de empleado No corresponde a un supervisor");
	}
}


function validarAccesoPromotor(iPosArray) {
	var arrEmpsupervisor = Array();
	var numempleado = document.getElementById('txtNumEmpleado').value;

	opcionPromotor = iPosArray;
	document.getElementById("Modaltst").style.display = 'block';
	document.getElementById("divSubMenu").style.display = 'none';


	iOpcion = VALIDA_PROMOTOR;
	document.getElementById("btnEntrar").onclick = function () {
		if (document.getElementById("txtNumEmpleado").value != '' && document.getElementById("txtNumEmpleado").value.length == 8) {
			cargarHuella();

		} else {
			mensajeTiempoMonitor("Debe capturar un número de empleado supervisor válido");
		}
	}

	document.getElementById("btnApagar").onclick = function () {
		document.getElementById("txtNumEmpleado").value = "";

		for (i = 0; i < arrAplicaciones.length; i++) {
			if (arrAplicaciones[i].nombre == 'apagarPc') {
				ejecutaWebService(arrAplicaciones[i].valor, '');
				i = arrAplicaciones.length;
			}
		}
		document.getElementById("Modaltst").style.display = 'none';
	}
	document.getElementById('txtNumEmpleado').onkeypress = function (evento) {
		var charCode = (evento.which) ? evento.which : evento.keyCode
		if (esTeclaNumerica(evento)) {
			if (charCode == 13) {
				var btnLogin = document.getElementById("btnEntrar");
				if (btnLogin)
					btnLogin.click();
			}
			return true;
		}
		return false;
	}
}

function validarAccesoSupervisores(iPosArray) {
	var arrEmpsupervisor = Array();
	var numempleado = document.getElementById('txtNumEmpleado').value;

	opcionSupervidores = iPosArray;
	document.getElementById("Modaltst").style.display = 'block';
	document.getElementById("divSubMenu").style.display = 'none';


	iOpcion = VALIDA_SUPERVISOR;
	document.getElementById("btnEntrar").onclick = function () {
		if (document.getElementById("txtNumEmpleado").value != '' && document.getElementById("txtNumEmpleado").value.length == 8) {
			cargarHuella();

		} else {
			mensajeTiempoMonitor("Debe capturar un número de empleado de supervisor válido");
		}
	}

	document.getElementById("btnApagar").onclick = function () {
		document.getElementById("txtNumEmpleado").value = "";

		for (i = 0; i < arrAplicaciones.length; i++) {
			if (arrAplicaciones[i].nombre == 'apagarPc') {
				ejecutaWebService(arrAplicaciones[i].valor, '');
				i = arrAplicaciones.length;
			}
		}
		document.getElementById("Modaltst").style.display = 'none';
	}
	document.getElementById('txtNumEmpleado').onkeypress = function (evento) {
		var charCode = (evento.which) ? evento.which : evento.keyCode
		if (esTeclaNumerica(evento)) {
			if (charCode == 13) {
				var btnLogin = document.getElementById("btnEntrar");
				if (btnLogin)
					btnLogin.click();
			}
			return true;
		}
		return false;
	}

}
/////////////////////////////////////////////////////////////////////
function mostrarEjericios(iEmpleado) {
	$.ajax({
			async: false,
			cache: false,
			url: "php/mostrarEjercicios.php",
			type: 'POST',
			dataType: 'json',
			data: { empleado: iEmpleado, consecutivo: 0, opcion: 1 },
			success: function(data) {
				arrDatos = eval(data);
				if (data != "") {
					if (arrDatos[0].url == "SIN REGISTROS" || arrDatos[0].url == "NO EXISTE EMPLEADO O ESTA DADO DE BAJA") {

					} else {
						iKeyx = arrDatos[0].keyx;
						mdlHtml(arrDatos[0].url);
					}
				}
			},
			error: function(a, b, c) {
				alert("error ajax " + a + " " + b + " " + c);
			}
		});
}

function actualizarEjercicio() {
	$.ajax({
			async: false,
			cache: false,
			url: "php/mostrarEjercicios.php",
			type: 'POST',
			dataType: 'json',
			data: { empleado: iEmpleado, keyx: iKeyx, opcion: 2 },
			success: function(data) {

			},
			error: function(a, b, c) {
				alert("error ajax " + a + " " + b + " " + c);
			}
		});
}

function ejecutarAplicacionInicio() {
	var sHtml = "";
	var i = 0; 
	var sApp = "";
	 var sParam = ""; 
	 var j = 0;
	  var sEsperar = 2;

	for (i = 0; i < arrEjecAppInicio.length; i++) {
		sEsperar = 2;
		if (arrEjecAppInicio[i].nombre == 'ftpm') {
			sApp = arrEjecAppInicio[i].valor;
			for (j = 0; j < arrServidores.length; j++) {
				if (arrServidores[j].nombre == 'ftp') {
					sEsperar = 3;
					sParam = arrServidores[j].valor + ' UpdatePc.exe';
					j = arrServidores.length;
				}
			}
		} else {
			sApp = arrEjecAppInicio[i].valor;
			sParam = '';
		}
		if (arrEjecAppInicio[i].nombre == 'descompresorFirefox52') {
			sParam = '1';
		}
		ejecutaWebService(sApp, sParam);
		
	}

}

/**************************************<WEBSERVICE EXE>**************************************/
/**
 * EJECUTA WEBSERVICE
 */
function ejecutaWebService(sRuta, sParametros) {
	var iEsperar = 2;
	soapData = "",
		httpObject = null,
		docXml = null,
		iEstado = 0,
		sMensaje = "";
	sUrlSoap = "http://127.0.0.1:20044/";

	if (iOpcion == IDENTIFICAR_EMP || iOpcion == VALIDA_SUPERVISOR || iOpcion == VALIDA_PROMOTOR)
		iEsperar = 1;

	soapData =
		'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
		'<SOAP-ENV:Envelope' +
		' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"' +
		' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"' +
		' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"' +
		' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"' +
		' xmlns:ns2=\"urn:ServiciosWebx\">' +
		'<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">' +
		'<ns2:ejecutarAplicacion>' +
		'<inParam>' +
		'<Esperar>' + iEsperar + '</Esperar>' +
		'<RutaAplicacion>' + sRuta + '</RutaAplicacion>' +
		'<parametros><![CDATA[' + sParametros + ']]></parametros>' +
		'</inParam>' +
		'</ns2:ejecutarAplicacion>' +
		'</SOAP-ENV:Body>' +
		'</SOAP-ENV:Envelope>';

	httpObject = getHTTPObject();

	if (httpObject) {
		if (httpObject.overrideMimeType) {
			httpObject.overrideMimeType("false");
		}

		httpObject.open('POST', sUrlSoap, false); //-- no asincrono
		httpObject.setRequestHeader("Accept-Language", null);
		httpObject.onreadystatechange = function() {
			if (httpObject.readyState == 4 && httpObject.status == 200) {
				parser = new DOMParser();
				docXml = parser.parseFromString(httpObject.responseText, "text/xml");
				iEstado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;
				//Llamada a la función que recibe la respuesta del WebService
				respuestaWebService(iEstado);
			} else {
				console.log(httpObject.status);
			}
		};
		httpObject.send(soapData);
	}
}

function getHTTPObject() {
	var xhr = false;
	if (window.ActiveXObject) {
		try {
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				xhr = false;
			}
		}
	} else if (window.XMLHttpRequest) {
		try {
			xhr = new XMLHttpRequest();
		} catch (e) {
			xhr = false;
		}
	}
	return xhr;
}

/**
 * RESPUESTA WEBSERVICE
 */
async function respuestaWebService(iRespuesta) {
	var iEmpCaptura = 0;
	switch (iOpcion) {
		case IDENTIFICAR_EMP:
			iEmpCaptura = document.getElementById('txtNumEmpleado').value;
			if (iRespuesta >= 90000000) {
				iEmpleado = iRespuesta;
				if (iEmpCaptura == iEmpleado) {
					consultarEmpleado(iEmpleado);

					if (iEstado == 1) {
						await generarToken(iEmpleado);
						if (!authorization) {
							mensaje(TIPO_MSJ_AVISO, "Sin conexión a servidor");
						} else {
							iIdPagina = PAG_MENU_BOTONES;
							cargarPagina();
							//EJECUCION PARA REPRODUCCION DEL VIDEO AL INICIAR EL MENUAFORE
							if (validarReproduccionVideo()) {
								espere();
								Concurrent.Thread.create(function() {
									descargaVideo();
									document.getElementById('divDlgEspere').innerHTML = "";
									document.getElementById('divDlgEspere').style.display = 'none';
									mdlVideo(cBase64);
									document.getElementById("viDecalogo").play();
									document.getElementById("viDecalogo").onended = function() {
										document.getElementById("divDlgVideo").innerHTML = "";
										document.getElementById("divDlgVideo").style.display = 'none';
										//funcion para actualizar registro de que ya vio el video
										actualizarReproduccionVideo();
										mostrarEjericios(iEmpleado);
									};
								});
							} else {
								mostrarEjericios(iEmpleado);
							}
							inicializarDivMenuBotones(iEmpleado);
						}
					}
				} else
					mensaje(TIPO_MSJ_AVISO, 'El número de empleado registrado no coincide con el de la huella');
			} else {
				switch (iRespuesta) {
					case '-1':
						mensaje(TIPO_MSJ_AVISO, "Sin conexión a servidor");
						break;
					case '0':
						break;
					case '1':
						mensaje(TIPO_MSJ_AVISO, "La huella del empleado no coincide");
						break;
					case '2':
						mensaje(TIPO_MSJ_AVISO, "El empleado no existe en la Base de Datos");
						break;
					case '3':
						mensaje(TIPO_MSJ_AVISO, "No esta instalado el software del sensor");
						break;
					case '4':
						mensaje(TIPO_MSJ_AVISO, "Existe una instancia activa");
						break;
					case '5':
						mensaje(TIPO_MSJ_AVISO, "La clave del empleado no corresponde con la huella");
						break;
					default:
						/*mensaje(TIPO_MSJ_AVISO,"Excepcion: "+iRespuesta);*/
						break;
				}
				document.getElementById("btnEntrar").disabled = false;
			}
			iOpcion = 0;
			break;
		case VALIDA_SUPERVISOR:
			iEmpCaptura = document.getElementById('txtNumEmpleado').value;
			if (iRespuesta >= 90000000) {
				iEmpleado = iRespuesta;
				if (iEmpCaptura == iEmpleado) {
					if (iEmpleado != '') {
						await generarToken(iEmpleado);
						consultarEmpleadosup(iEmpleado);
					} else {
						mensajeTiempoMonitor("El número de empleado no pertenece al supervisor");
					}
				}
				else
					mensaje(TIPO_MSJ_AVISO, 'El número de empleado registrado no coincide con el de la huella');
			}
			else {
				switch (iRespuesta) {
					case '-1': mensaje(TIPO_MSJ_AVISO, "Sin conexión a servidor"); break;
					case '0': break;
					case '1': mensaje(TIPO_MSJ_AVISO, "La huella del empleado no coincide"); break;
					case '2': mensaje(TIPO_MSJ_AVISO, "El empleado no existe en la Base de Datos"); break;
					case '3': mensaje(TIPO_MSJ_AVISO, "No esta instalado el software del sensor"); break;
					case '4': mensaje(TIPO_MSJ_AVISO, "Existe una instancia activa"); break;
					case '5': mensaje(TIPO_MSJ_AVISO, "La clave del empleado no corresponde con la huella"); break;
					default: /*mensaje(TIPO_MSJ_AVISO,"Excepcion: "+iRespuesta);*/ break;
				}
				document.getElementById("btnEntrar").disabled = false;
			}
			iOpcion = 0;
			break;

		case VALIDA_PROMOTOR:
			iEmpCaptura = document.getElementById('txtNumEmpleado').value;
			if (iRespuesta >= 90000000) {
				iEmpleado = iRespuesta;
				if (iEmpCaptura == iEmpleado) {
					if (iEmpleado != '') {
						await generarToken(iEmpleado);
						consultarPromotor(iEmpleado);

					} else {
						alert("El número de empleado no pertenece al promotor");
					}
				}
				else
					mensaje(TIPO_MSJ_AVISO, 'El número de empleado registrado no coincide con el de la huella');
			}
			else {
				switch (iRespuesta) {
					case '-1': mensaje(TIPO_MSJ_AVISO, "Sin conexión a servidor"); break;
					case '0': break;
					case '1': mensaje(TIPO_MSJ_AVISO, "La huella del empleado no coincide"); break;
					case '2': mensaje(TIPO_MSJ_AVISO, "El empleado no existe en la Base de Datos"); break;
					case '3': mensaje(TIPO_MSJ_AVISO, "No esta instalado el software del sensor"); break;
					case '4': mensaje(TIPO_MSJ_AVISO, "Existe una instancia activa"); break;
					case '5': mensaje(TIPO_MSJ_AVISO, "La clave del empleado no corresponde con la huella"); break;
					default: /*mensaje(TIPO_MSJ_AVISO,"Excepcion: "+iRespuesta);*/ break;
				}
				document.getElementById("btnEntrar").disabled = false;
			}
			iOpcion = 0;
			break;
		default:
			break;
	}
}


function generarToken(iEmpleado) {

	return new Promise(resolve => {

		var nroSesiones = 0;

		$.ajax({
			async: false,
			cache: false,
			data: { empleado: iEmpleado, iOpcion: 1 },
			url: 'php/CSesion.php',
			type: 'POST',
			dataType: 'json',
			success: function(data) {

				bRetorna = data.result;
				nroSesiones = bRetorna.session;
				if (nroSesiones == 1) {
					mensajeSesion(TIPO_MSJ_AVISO, "PROMOTOR: Ya cuentas con una sesión iniciada en otro dispositivo ¿Deseas cerrarla y continuar?");
				} else {
					localStorage.setItem('tokenAfore', JSON.stringify(bRetorna));
					authorization = JSON.parse(localStorage.getItem('tokenAfore')).token;
					resolve();
				}

			},
			error: function(xhr, status, error) {
				resolve();
			},
		});
	});
}

function cerrarSesion(iEmpleado) {

	$.ajax({
		async: false,
		cache: false,
		data: { empleado: iEmpleado, iOpcion: 2 },
		url: 'php/CSesion.php',
		type: 'POST',
		dataType: 'json',
		success: function(data) {
			bRetorna = data.result;
			if (bRetorna == true) {
				localStorage.removeItem('tokenAfore');
			}
		},
		error: function(xhr, status, error) {},
	});
}

function mensajeSesion(iTipo, sMensaje) {
	var myDlg = new dialog_ac(document.getElementById('divDlgMensaje'));
	myDlg.mensajeSesion(600, 200, 'MENU AFORE');
	myDlg.mostrar(sMensaje);
}