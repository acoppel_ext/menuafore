<?php
include("global.php");
class CGenerales
{
	function __construct()
	{
		date_default_timezone_set('America/Mexico_City');
	}
	function __destruct()
	{
	}

	public function getIpRemoto()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
           
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
       
        return $_SERVER['REMOTE_ADDR'];
	}
	public function fechaYMD()
	{
		return date("Ymd");
	}
	public function fechaCompleta()
	{
		global $arrDias;
		global $arrMeses;
		return $arrDias[date("N")-1] . ', '. date("d") . ' de ' . $arrMeses[date("m")-1] . ' de '. date("Y");
	}
	public function grabarLogx($cadLogx)
	{
		$cIpCliente = $this->getIpRemoto();
		$rutaLog =  RUTA_LOGX .  '-' . date("d") . '-' . date("m") . '-' . date("Y") . ".txt"; 
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}
}

?>