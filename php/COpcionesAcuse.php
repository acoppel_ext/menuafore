<?php
	include_once('CGenerales.php');
	include_once('JSON.php');
	
	$json 					= new Services_JSON();
	$objGn 					= new CGenerales();
	$arrDatos 				= array();

	$iOpcion 				= isset($_POST['opcion']) 						? $_POST['opcion'] 						: 0;
	$iOpcionAcuseGenerado	= isset($_POST['opcionAcuseGenerado']) 			? $_POST['opcionAcuseGenerado'] 		: 0;
	$iEmpleado				= isset($_POST['empleado']) 					? $_POST['empleado'] 					: 0;
	$cNss					= isset($_POST['nss']) 							? $_POST['nss'] 						: '';
	$iCveConsar				= isset($_POST['cveconsar']) 					? $_POST['cveconsar'] 					: 0;
	$cNombres				= isset($_POST['nombres']) 						? $_POST['nombres'] 					: '';
	$cApePaterno			= isset($_POST['apepat']) 						? $_POST['apepat'] 						: '';
	$cApeMaterno			= isset($_POST['apemat']) 						? $_POST['apemat'] 						: '';
	$cCurp					= isset($_POST['curp']) 						? $_POST['curp'] 						: '';
	$cFecharegistro			= isset($_POST['fecharegistro'])			 	? $_POST['fecharegistro'] 				: '';
	$iTienda				= isset($_POST['tienda']) 						? $_POST['tienda'] 						: 0;
	
	switch($iOpcion)
	{
        case 1:
			try
			{
				$cnxBd =  new PDO( "pgsql:host=".IP_BD_AFO.";port=5432;dbname=".BD_AFO, USR_BD_AFO, PWD_BD_AFO);
				if($cnxBd)
				{
					$cSql = "select fun_validacveconsarvigente from fun_validacveconsarvigente(".$iEmpleado.");";
					$objGn->grabarLogx("[fun_validacveconsarvigente] -->".$cSql);
					$resulSet = $cnxBd->query($cSql);
					if($resulSet)
					{
						foreach($resulSet as $reg)
						{	
							$arrDatos["respuesta"] 	= $reg["fun_validacveconsarvigente"];
						}
					}else{
						$arrErr = $cnxBd->errorInfo();
						$objGn->grabarLogx( 'Error en la consulta [fun_validacveconsarvigente]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
					}
				}else{
					$arrErr = $cnxBd->errorInfo();
					$objGn->grabarLogx( 'Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}catch (Exception $e){
				$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
			$cnxBd = null;
			break;
		case 2:
			try
			{
				$cnxBd =  new PDO( "pgsql:host=".IPIMAGENES.";port=5432;dbname=".BDIMAGENES, USERIMAGENES, PASSIMAGENES);
				if($cnxBd)
				{
					$cSql = "select fun_opcionesacusedeetica from fun_opcionesacusedeetica(".$iOpcionAcuseGenerado.",'','".$cNss."','".$iCveConsar."');";
					$objGn->grabarLogx("[fun_opcionesacusedeetica] -->".$cSql);
					$resulSet = $cnxBd->query($cSql);
					if($resulSet)
					{
						foreach($resulSet as $reg)
						{	
							$arrDatos["respuesta"] 	= $reg["fun_opcionesacusedeetica"];
						}
					}else{
						$arrErr = $cnxBd->errorInfo();
						$objGn->grabarLogx( 'Error en la consulta [fun_opcionesacusedeetica]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
					}
				}else{
					$arrErr = $cnxBd->errorInfo();
					$objGn->grabarLogx( 'Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}catch (Exception $e){
				$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
			$cnxBd = null;
			break;
		case 3:
			try
			{
				$cIp= $objGn->getIpRemoto();
				$cnxBd =  new PDO( "pgsql:host=".IP_BD_AFO.";port=5432;dbname=".BD_AFO, USR_BD_AFO, PWD_BD_AFO);
				if($cnxBd)
				{
					$cSql ="select fun_guardaracuseetica from fun_guardaracuseetica(1,".$iEmpleado.",'".$cNombres."','".$cApePaterno."','".$cApeMaterno."','".$cCurp."','".$cNss."',".$iCveConsar.",'".$cFecharegistro."'::date,'".$cIp."',".$iTienda.");";
					$objGn->grabarLogx("[fun_guardaracuseetica] -->".$cSql);
					$resulSet = $cnxBd->query($cSql);
					if($resulSet)
					{
						foreach($resulSet as $reg)
						{	
							$arrDatos["respuesta"] 			= $reg["fun_guardaracuseetica"];
						}
					}else{
						$arrErr = $cnxBd->errorInfo();
						$objGn->grabarLogx( 'Error en la consulta [fun_guardaracuseetica]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
					}
				}else{
					$arrErr = $cnxBd->errorInfo();
					$objGn->grabarLogx( 'Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}catch (Exception $e){
				$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
			$cnxBd = null;
			break;
		case 4:
			try
			{
				$cnxBd =  new PDO( "pgsql:host=".IP_BD_AFO.";port=5432;dbname=".BD_AFO, USR_BD_AFO, PWD_BD_AFO);
				if($cnxBd)
				{
					
					$cSql = "select nombres,apepaterno,apematerno,curp,nss,claveconsar as cveconsar,fecharegistro ,tienda from fun_consultardatoscolinfxpromotores(".$iEmpleado.");";
					$objGn->grabarLogx("[fun_consultardatoscolinfxpromotores] -->".$cSql);
					$resulSet = $cnxBd->query($cSql);
					if($resulSet)
					{
						foreach($resulSet as $reg)
						{	
							$arrDatos["nombres"] 			= $reg["nombres"];
							$arrDatos["apepaterno"] 		= $reg["apepaterno"];
							$arrDatos["apematerno"] 		= $reg["apematerno"];
							$arrDatos["curp"] 				= $reg["curp"];
							$arrDatos["nss"] 				= $reg["nss"];
							$arrDatos["cveconsar"] 			= $reg["cveconsar"];
							$arrDatos["fecharegistro"] 		= $reg["fecharegistro"];
							$arrDatos["tienda"] 			= $reg["tienda"];
						}
					}else{
						$arrErr = $cnxBd->errorInfo();
						$objGn->grabarLogx( 'Error en la consulta [fun_consultardatoscolinfxpromotores]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
					}
				}else{
					$arrErr = $cnxBd->errorInfo();
					$objGn->grabarLogx( 'Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}catch (Exception $e){
				$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
			$cnxBd = null;
			break;			
	}
echo $json->encode($arrDatos);
?>