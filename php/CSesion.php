<?php

include_once('CapirestAuth.php');
session_start();
$iEmpleado =  isset($_POST['empleado']) ? $_POST['empleado'] : '0';
$iOpcion =  isset($_POST['iOpcion']) ? $_POST['iOpcion'] : '0';

$result = array();

if ($iEmpleado) {

    switch ($iOpcion) {
        case 1:
            $result = CSesion::inicioSesion($iEmpleado);
            break;
        case 2:
            $result = CSesion::cerrarSesion($iEmpleado);
            session_destroy();
            break;
        default:

            break;
    }
}

echo json_encode($result);


class CSesion
{

    public static function inicioSesion($iEmpleado)
    {

        $result = CapirestAuth::consumirApi(
            'RegistroToken',
            array(
                'codigoEmpleado' => $iEmpleado,
                'modulo' => 1
            ),
            "CtrlAuth"
        );

        if ($result->result->session == 0) {
            $_SESSION["authorization"] = CSesion::decrypt($result->result->token, 'aqwsedrftgyhujikolp12');
            $_SESSION["empleado"] = $iEmpleado;
        }

        return $result;
    }

    public static function cerrarSesion($iEmpleado)
    {

        return CapirestAuth::consumirApi(
            'eliminarToken',
            array('codigoEmpleado' => $iEmpleado),
            "CtrlAuth"
        );
    }
    public static function decrypt($string, $key)
    {
        $result = '';
        $string = base64_decode($string);
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) - ord($keychar));
            $result .= $char;
        }
        return $result;
    }
}
