<?php

	include_once("CGenerales.php");
	include_once("JSON.php");
	$objGn = new CGenerales();
	$json = new Services_JSON();
	$arrsupervisor=array();
	$iEmpleado=0;
	$sIpRemoto='';
	$iOpcion = 0;
	

	$iEmpleado=$_POST['numempleadosup'];


	if(isset($_POST['numempleadosup']))
	{		
				
		$arrsupervisor=consultasupervisor($iEmpleado);		
		$sIpRemoto=$objGn->getIpRemoto();	
		
	}
	else
	{
		$arrsupervisor['estado']=ERR_PARAM;
		$arrsupervisor['descripcion']=MSJ_ERR_PARAM;
		$objGn->grabarLogx(MSJ_ERR_PARAM);
	}

	echo $json->encode($arrsupervisor);
	
	function consultasupervisor($iEmpleado)
	{
		global $objGn;
		global $sIpRemoto;
		$arrsupervisor=array('estado'=>0, 'descripcion'=>'', 'empleadovalidadosup'=>'');

		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IPCAPACITACIONAFORE.";port=5432;dbname=".BASEDEDATOSCAPACITACIONAFORE, USUARIOCAPACITACIONAFOREBD, PASSWORDCAPACITACIONAFORE);
			if($cnxBd)
			{
				$sSql="SELECT tbnumempasp , tbnombretrabajador, tbcurp, tbclaveproctor, tbfechaexamen, tbhoraprogramacion, tbtienda , tbregion, tbnumrmpsup,tbnomempsupreg, tbhoraregistro, tbestatus 
					FROM funeicargarclavesproctor(6 ,$iEmpleado,'','','','','',0,'','','');";
				//var_dump($sSql);	
				$objGn->grabarLogx($sSql);
				$resulSet = $cnxBd->query($sSql);
				if($resulSet) 
				{ 
					$arrsupervisor['estado'] = OK__;
					$arrsupervisor['descripcion'] = MSJ_EXITO;
					foreach($resulSet as $reg) 
					{
						if($reg['tbnumrmpsup'] !=0)
						{
							$arrsupervisor['empleadovalidadosup']=  trim($reg['tbnumrmpsup']);
						}
						else
						{
							$arrsupervisor['empleadovalidadosup']=0;
						
						}
					}
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$arrsupervisor['estado'] = ERR_EXEC_CON_SQL;
					$arrsupervisor['descripcion'] = MSJ_ERR_EXEC_CON_SQL;
					$objGn->grabarLogx(' Error consultar empleado: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				$arrsupervisor['estado'] = ERR_CNX_BD;
				$arrsupervisor['descripcion'] = MSJ_ERR_CNX_BD;
				$objGn->grabarLogx(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			$cnxBd = null;
		}
		catch(PDOException $ex)
		{
			$arrsupervisor['estado'] = ERR__;
			$arrsupervisor['descripcion'] = MSJ_EXCEP_;
			$objGn->grabarLogx($ex->getMessage());
		}
		return $arrsupervisor;
	}	

?>