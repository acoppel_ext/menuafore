<?php
	include_once('clases/cbase.php');

	$arrResp 	= array();
	//Variables Generales
	$opcion 		= isset($_POST['opcion']) 	? $_POST['opcion'] 		: 0;
	$sTexto 		= isset($_POST['mensaje']) 	? $_POST['mensaje'] 	: '';
	$idCons 		= isset($_POST['idcons']) 	? $_POST['idcons'] 		: '';
	$arrOpciones 	= isset($_POST['arrdatos']) ? $_POST['arrdatos'] 	: '';
	
	switch ($opcion) {
		case 1: $arrResp = CGeneral::grabarRegistro($sTexto); break;
		case 2: $arrResp = CGeneral::datosPagina(); break;
		case 3: $arrResp = CGeneral::consultasProyectos($idCons, $arrOpciones); break;
	}

	echo json_encode($arrResp);
?>