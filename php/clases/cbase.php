<?php
include_once("../../distboostrap4/php/definiciones.php");

class CGeneral extends CMetodoGral
{
	public static $cNombreLog = "menuafore";
	
	//OPCION 1
	public static function grabarRegistro($texto)
	{
		//dar el nombre del log
		CMetodoGral::setLogName(CGeneral::$cNombreLog);
		CMetodoGral::grabarLogx("[" . __METHOD__ . "]" . $texto);
		$arrDatos = array("texto" => $texto);
		return $arrDatos;
	}

	//OPCION 2
	public static function datosPagina(){
		$arrDatos 	= array(
			"irespuesta" 	=> CONSOK__,
			"fechaActual"	=> "",
			"ipspa"			=> "",
			"ipmodulo"		=> ""
		);
		
		//Obtenemos la fecha actual y se la mandamos como parametro
		setlocale(LC_TIME, "es_ES");
		$cFechaDia 					= utf8_encode(strftime("%A, %d de %B de %Y"));
		$arrDatos["fechaActual"] 	= $cFechaDia;
		$arrBD 						= CMetodoGral::getDatosBD("AFOREGLOBAL");
		$arrDatos["ipspa"]			= $arrBD["AFOREGLOBAL"]["BDIP"];
		$arrDatos["ipmodulo"]		= CMetodoGral::getRealIP();

		return $arrDatos;
	}

	public static function consultasProyectos($idCons, $arrOpciones)
	{
		$arrResp = array("irespuesta" => DEFAULT__, $idCons => null);
		$arrCons 	= array(
			"CONS00" => array(
				"BDNM" => "AFOREGLOBAL", 
				"CONS" => "SELECT exeid,exeliga,exeparametros,exeestatus FROM fnobtenermnuejecutables();"
			),
			"CONS01" => array(
				"BDNM" => "AFOREGLOBAL", 
				"CONS" => "SELECT empleado,TRIM(nombre || ' ' || apellidopat || ' ' || apellidomat) AS nombre,centro,TRIM(nombrecentro) AS nombrecentro,claveconsar,curp,contrasenia,passsaftv,tienda FROM fnmnuConsultarEmpleado(<<DATOS>>);"
			),
			"CONS02" => array(
				"BDNM" => "AFOREGLOBAL", 
				"CONS" => "SELECT iretorno, ivideo FROM fnvalidarreproduccionvideomodulo(<<DATOS>>);"
			),
			"CONS03" => array(
				"BDNM" => "AFOREGLOBAL", 
				"CONS" => "SELECT fndescargavideo AS video FROM fndescargavideo(<<DATOS>>);"
			),
			"CONS04" => array(
				"BDNM" => "AFOREGLOBAL", 
				"CONS" => "SELECT fechaalta AS fecha, TRIM(replace(descripcion,'h3','h6')) AS mensaje FROM fnmnuconsultarmensaje01(<<DATOS>>) ORDER BY fechaalta DESC;"
			),
			"CONS05" => array(
				"BDNM" => "AFOREGLOBAL", 
				"CONS" => "SELECT irespuesta, TRIM(cmensaje) AS cmensaje, TRIM(cmetodo) AS accion FROM fnvalidarrevalidacionapacceso(<<DATOS>>);"
			),
			"CONS06" => array(
				"BDNM" => "AFOREGLOBAL", 
				"CONS" => "SELECT idopcion,control,subcontrol,TRIM(descripcion) AS descripcion,liga,tipoaplicacion,tiponavegador,grupo FROM fnmnuconsultaropciones03(<<DATOS>>) ORDER BY descripcion;"
			),
			"CONS07" => array(
				"BDNM" => "AFOREGLOBAL", 
				"CONS" => "SELECT fnmonitorafiliacionpromotortime AS irespuesta FROM fnmonitorafiliacionpromotortime(<<DATOS>>);"
			)
		);

		//Validar que la consulta seleccionada exista
		if (array_key_exists($idCons, $arrCons)) {
			$arrResp["irespuesta"] 	= CONSFOUNDOK__;
			$arrCons 				= $arrCons[$idCons];
			$arrCons["CONS"]		= str_replace("<<DATOS>>", $arrOpciones, $arrCons["CONS"]);
			CMetodoGral::grabarLogx("[" . __METHOD__ . "] CONS->" . $idCons . " BDNM-> " . $arrCons["BDNM"] . " CONS-> " . $arrCons["CONS"]);
			
			//Ejecutar la consulta obtenida
			$arrConsulta = CMetodoGral::ejecucionBD($idCons, $arrCons);
			if($arrConsulta["irespuesta"] == CONSOK__){
				$arrResp["irespuesta"] 	= $arrConsulta["irespuesta"];
				$arrResp[$idCons] 		= $arrConsulta[$idCons];
			}
			
		} else {
			$arrResp["irespuesta"] = CONSFOUNDERR__;
			CMetodoGral::grabarLogx("[" . __METHOD__ . "] No encuentra la consulta: " . $idCons);
		}

		return $arrResp;
	}
}
?>