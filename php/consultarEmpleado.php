<?php

	include_once("CGenerales.php");
	include_once("JSON.php");
	$objGn = new CGenerales();
	$json = new Services_JSON();
	$arrEmpl=array();
	$iEmpleado=0;
	$sIpRemoto='';
	if(isset($_POST['empleado']))
	{
		$sIpRemoto=$objGn->getIpRemoto();
		$iEmpleado=$_POST['empleado'];
		$arrEmpl=consultarEmpleado($iEmpleado);
	}
	else
	{
		$arrEmpl['estado']=ERR_PARAM;
		$arrEmpl['descripcion']=MSJ_ERR_PARAM;
		$objGn->grabarLogx(MSJ_ERR_PARAM);
	}
	echo $json->encode($arrEmpl);
	function consultarEmpleado($iEmpleado)
	{
		global $objGn;
		global $sIpRemoto;
		$datosEmpl=array('estado'=>0, 'descripcion'=>'', 'datosempleado'=>array());
		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IP_BD_AFO.";port=5432;dbname=".BD_AFO, USR_BD_AFO, PWD_BD_AFO);
			if($cnxBd)
			{
				$sSql="select empleado,nombre,apellidopat,apellidomat,centro,nombrecentro,claveconsar,curp,contrasenia,passsaftv,tienda from fnmnuConsultarEmpleado(".$iEmpleado.",'". $sIpRemoto ."');";
				$objGn->grabarLogx($sSql);
				$resulSet = $cnxBd->query($sSql);
				if($resulSet) 
				{ 
					$datosEmpl['estado'] = OK__;
					$datosEmpl['descripcion'] = MSJ_EXITO;
					foreach($resulSet as $reg) 
					{
						$datosEmpl['datosempleado'][]=  array_map('trim', $reg);
					}
						//Cambira la � para apellido Paterno
						$sTexto = $datosEmpl["datosempleado"][0]["apellidopat"];
						//Itera la cadena para cambiar la letra �
						for($i = 1; $i < strlen($sTexto); $i++ )
						{
							if( $sTexto[$i] == '?' ||  $sTexto[$i] == '�' ||  $sTexto[$i] == '#')
							{
								$sTexto[$i] = '�';
							}		
						}
						$sTexto = ucwords(strtolower($sTexto));
						$datosEmpl["datosempleado"][0]["apellidopat"] = utf8_encode($sTexto);
						
						//Cambira la � para apellido Materno
						$sTextoMaterno = $datosEmpl["datosempleado"][0]["apellidomat"];
						//Itera la cadena para cambiar la letra �
						for($i = 1; $i < strlen($sTextoMaterno); $i++ )
						{
							if( $sTextoMaterno[$i] == '?' ||  $sTextoMaterno[$i] == '�' ||  $sTextoMaterno[$i] == '#')
							{
								$sTextoMaterno[$i] = '�';
							}		
						}
						$sTextoMaterno = ucwords(strtolower($sTextoMaterno));
						$datosEmpl["datosempleado"][0]["apellidomat"] = utf8_encode($sTextoMaterno);
						
						//Cambira la � para Nombre
						$sTextoNombre = $datosEmpl["datosempleado"][0]["nombre"];
						//Itera la cadena para cambiar la letra �
						for($i = 1; $i < strlen($sTextoNombre); $i++ )
						{
							if( $sTextoNombre[$i] == '?' ||  $sTextoNombre[$i] == '�' ||  $sTextoNombre[$i] == '#')
							{
								$sTextoNombre[$i] = '�';
							}		
						}
						$sTextoNombre = ucwords(strtolower($sTextoNombre));
						$datosEmpl["datosempleado"][0]["nombre"] = utf8_encode($sTextoNombre);
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$datosEmpl['estado'] = ERR_EXEC_CON_SQL;
					$datosEmpl['descripcion'] = MSJ_ERR_EXEC_CON_SQL;
					$objGn->grabarLogx(' Error consultar empleado: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				$datosEmpl['estado'] = ERR_CNX_BD;
				$datosEmpl['descripcion'] = MSJ_ERR_CNX_BD;
				$objGn->grabarLogx(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			$cnxBd = null;
		}
		catch(PDOException $ex)
		{
			$datosEmpl['estado'] = ERR__;
			$datosEmpl['descripcion'] = MSJ_EXCEP_;
			$objGn->grabarLogx($ex->getMessage());
		}
		
		return $datosEmpl;
	}

?>