<?php
	include_once("CGenerales.php");
	include_once("JSON.php");
	$objGn = new CGenerales();
	$json = new Services_JSON();
	$arrMensajes=array();
	$iOpcion=0;
	if(isset($_POST['opcion']))
	{
		$iOpcion=$_POST['opcion'];
		$arrMensajes=consultarMensaje($iOpcion);
	}
	else
	{
		$arrMensajes['estado']=ERR_PARAM;
		$arrMensajes['descripcion']=MSJ_ERR_PARAM;
		$objGn->grabarLogx(MSJ_ERR_PARAM);
	}
	echo $json->encode($arrMensajes);
	function consultarMensaje($iOpcion)
	{
		global $objGn;
		$datosMensaje=array('estado'=>0, 'descripcion'=>'', 'mensajes'=>array());
		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IP_BD_AFO.";port=5432;dbname=".BD_AFO, USR_BD_AFO, PWD_BD_AFO);
			if($cnxBd)
			{
				$sSql="SELECT fechaalta, descripcion, empleado, estatus FROM fnmnuConsultarMensaje(".$iOpcion.") ORDER BY fechaalta DESC";
				$resulSet = $cnxBd->query($sSql);
				if($resulSet) 
				{ 
					foreach($resulSet as $reg) 
					{
						$datosMensaje['mensajes'][]= array_map('utf8_encode', $reg);
					}
					if(count($datosMensaje['mensajes'])>0)
					{
						$datosMensaje['estado'] = OK__;
						$datosMensaje['descripcion'] = MSJ_EXITO;
					}
					else
					{
						$datosMensaje['estado'] = ERR_NO_HAY_REG;
						$datosMensaje['descripcion'] = MSJ_ERR_NO_HAY_REG;
					}
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$datosEmpl['estado'] = ERR_EXEC_CON_SQL;
					$datosEmpl['descripcion'] = MSJ_ERR_EXEC_CON_SQL;
					$objGn->grabarLogx(' Error consultar mensajes: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				$datosMensaje['estado'] = ERR_CNX_BD;
				$datosMensaje['descripcion'] = MSJ_ERR_CNX_BD;
				$objGn->grabarLogx(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			$cnxBd = null;
		}
		catch(PDOException $ex)
		{
			$datosMensaje['estado'] = ERR__;
			$datosMensaje['descripcion'] = MSJ_EXCEP_;
			$objGn->grabarLogx($ex->getMessage());
		}
		return $datosMensaje;
	}
?>