<?php
	include_once("CGenerales.php");
	header("Content-Type: text/html;charset=utf-8");
	$objGn = new CGenerales();
	$arrOpciones=array();
	$iEmpleado=0;
	$sIpRemoto='';
	if(isset($_POST['empleado']))
	{
		$sIpRemoto=$objGn->getIpRemoto();
		$iEmpleado=$_POST['empleado'];
		$arrOpciones=consultarMenuOpciones($iEmpleado);
	}
	else
	{
		$arrOpciones['estado']=ERR_PARAM;
		$arrOpciones['descripcion']=MSJ_ERR_PARAM;
		$objGn->grabarLogx(MSJ_ERR_PARAM);
	}
	echo JSON_encode($arrOpciones);

	function consultarMenuOpciones($iEmpleado)
	{
		global $objGn;
		global $sIpRemoto;
		$iAccion = true;
		$datosOpcion=array('estado'=>0, 'descripcion'=>'', 'metodo'=>'', 'opciones'=>array());
		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IP_BD_AFO.";port=5432;dbname=".BD_AFO, USR_BD_AFO, PWD_BD_AFO);
			if($cnxBd)
			{
				$respuesta 	= array();
				$sSql 		= "SELECT irespuesta, TRIM(cmensaje) AS cmensaje, TRIM(cmetodo) AS cmetodo FROM fnvalidarrevalidacionapacceso(".$iEmpleado.");";
				$resulSet 	= $cnxBd->query($sSql);

				$objGn->grabarLogx("fnvalidarrevalidacionapacceso-->".$sSql);

				if($resulSet) 
				{ 
					foreach($resulSet as $reg) 
					{
						$datosOpcion['estado'] 		= $reg['irespuesta'];
						$datosOpcion['descripcion'] =  utf8_encode($reg['cmensaje']);
						$datosOpcion['metodo'] 		= $reg['cmetodo'];
						$datosOpcion['numEmpleado'] = $iEmpleado;
					}
				}
				else {
					$iAccion = false;
					$arrErr = $cnxBd->errorInfo();
					$datosEmpl['estado'] = ERR_EXEC_CON_SQL;
					$datosEmpl['descripcion'] = MSJ_ERR_EXEC_CON_SQL;
					$objGn->grabarLogx(' Error consultar fnvalidarrevalidacionapacceso: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
					
				if($iAccion){
					$sSql="SELECT idopcion,control,subcontrol,descripcion,liga,tipoaplicacion,tiponavegador,grupo FROM fnmnuConsultarOpciones02($iEmpleado,'$sIpRemoto') ORDER BY 1;";
					$objGn->grabarLogx("fnmnuConsultarOpciones02-->".$sSql);
					$resulSet = $cnxBd->query($sSql);
					if($resulSet){
						foreach($resulSet as $reg) 
						{
							$reg=array_map('trim',$reg);
							$datosOpcion['opciones'][]= array_map('trim', $reg);
						}
					}
					else
					{
						$arrErr = $cnxBd->errorInfo();
						$datosEmpl['estado'] = ERR_EXEC_CON_SQL;
						$datosEmpl['descripcion'] = MSJ_ERR_EXEC_CON_SQL;
						$objGn->grabarLogx(' Error consultar fnmnuConsultarOpciones02: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
					}
				}		
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				$datosOpcion['estado'] = ERR_CNX_BD;
				$datosOpcion['descripcion'] = MSJ_ERR_CNX_BD;
				$objGn->grabarLogx(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			$cnxBd = null;	
		}	
		catch(PDOException $ex)
		{
			$datosOpcion['estado'] = 0;
			$datosOpcion['descripcion'] = MSJ_EXCEP_;
			$objGn->grabarLogx($ex->getMessage());
		}
		return $datosOpcion;
	}
?>