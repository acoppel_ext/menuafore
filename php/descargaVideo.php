<?php

	include_once("CGenerales.php");
	include_once("JSON.php");
	$objGn = new CGenerales();
	$json = new Services_JSON();
	$arrDescargaVideo=array();
	$id=0;
	if(isset($_POST['id'])){
		$id = $_POST['id'];
		$arrDescargaVideo=consultarDescargaVideo($id);
	}
	else
	{
		$arrReproduccionVideo['estado']=ERR_PARAM;
		$arrReproduccionVideo['descripcion']=MSJ_ERR_PARAM;
		$objGn->grabarLogx(MSJ_ERR_PARAM);
	}
	
	echo $json->encode($arrDescargaVideo);
	
	function consultarDescargaVideo($id)
	{
		global $objGn;
		global $sIpRemoto;
		$respDescVideo=array('estado'=>0, 'descripcion'=>'', 'respuesta'=>'');
		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IP_BD_AFO.";port=5432;dbname=".BD_AFO, USR_BD_AFO, PWD_BD_AFO);
			if($cnxBd)
			{
				$sSql="SELECT fndescargavideo FROM fndescargavideo(".$id.")";
				$objGn->grabarLogx($sSql);
				$resulSet = $cnxBd->query($sSql);
				if($resulSet) 
				{ 
					$respDescVideo['estado'] = OK__;
					$respDescVideo['descripcion'] = MSJ_EXITO;
					foreach($resulSet as $reg) 
					{
						$objGn->grabarLogx('imagen de descargar' + $reg['fndescargavideo']);
						$respDescVideo['fndescargavideo'] =  trim($reg['fndescargavideo']);
						
					}
					$objGn->grabarLogx('[consultarDescargaVideo]Respuesta -> ' . $respDescVideo['respuesta']);
						
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$respDescVideo['estado'] = ERR_EXEC_CON_SQL;
					$respDescVideo['descripcion'] = MSJ_ERR_EXEC_CON_SQL;
					$objGn->grabarLogx('[consultarDescargaVideo] Error consultar empleado: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				$respDescVideo['estado'] = ERR_CNX_BD;
				$respDescVideo['descripcion'] = MSJ_ERR_CNX_BD;
				$objGn->grabarLogx('[consultarDescargaVideo] Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			$cnxBd = null;
		}
		catch(PDOException $ex)
		{
			$respDescVideo['estado'] = ERR__;
			$respDescVideo['descripcion'] = MSJ_EXCEP_;
			$objGn->grabarLogx($ex->getMessage());
		}
		
		return $respDescVideo;
	}
?>