<?php
	define('WEB_CONFIG', "webconfig.xml");
	define('RUTA_LOGX',	 "/sysx/progs/log/menuafore");

	define('OK__',     				  1);
	define('ERR_NO_EXISTE_ARCH',     -1);
	define('ERR_FTO_XML',		     -2);
	define('ERR_EMPL_NO_REG',		 -3);
	define('ERR_CNX_BD',			 -4);
	define('ERR_EXCEP',				 -5);
	define('ERR_PARAM',				 -6);
	define('ERR_EXEC_CON_SQL',		 -7);
	define('ERR_NO_HAY_REG',		 -8);

	define('MSJ_EXITO', "Proceso ejecutado con exito");
	define('MSJ_EMPL_NO_REG', 'Empleado no se encuentra registrado');
	define('MSJ_ERR_CNX_BD', "No se estableció conexión a la base de datos");
	define('MSJ_EXCEP_', "Excepcion en proceso");
	define('MSJ_ERR_PARAM', "Error en parametros");
	define('MSJ_ERR_EXEC_CON_SQL', "Error al consultar información en la base de datos");
	define('MSJ_ERR_NO_HAY_REG', "No se encontró información");
	
	$arrMeses=array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
	$arrDias=array('Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo');

	$configBd = simplexml_load_file("../../conf/webconfig.xml");
	define('IP_BD_AFO', $configBd->Spa);
	define('USR_BD_AFO', $configBd->Usuario);
	define('BD_AFO', $configBd->Basededatos);
	define('PWD_BD_AFO', $configBd->Password);
	
	define('IPCAPACITACIONAFORE', $configBd->IpCapacitacionAfore);
	define('BASEDEDATOSCAPACITACIONAFORE', $configBd->BasedeDatosCapacitacionAfore);
	define('USUARIOCAPACITACIONAFOREBD', $configBd->UsuarioCapacitacionAforeBD);
	define('PASSWORDCAPACITACIONAFORE', $configBd->PasswordCapacitacionAfore);	

	define('IPIMAGENES',          $configBd->IpImagenes);
	define('USERIMAGENES',        $configBd->UsuarioImg);
	define('BDIMAGENES',          $configBd->BasededatosImg);
	define('PASSIMAGENES',        $configBd->PasswordImg);
?>