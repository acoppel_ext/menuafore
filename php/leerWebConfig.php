<?php
	include_once("CGenerales.php");
	$objGn = new CGenerales();
	$arrResp=array('estado' => 0, 'IpLocal'=>'', 'fechaAMD'=>'', 'fecha'=>'','EjecutarAplicacionInicio'=>array(), 'Servidores'=>array(), 'aplicaciones'=>array());
	
	$arrResp['fechaAMD']=$objGn->fechaYMD();
	$arrResp['fecha']= $objGn->fechaCompleta();
	$arrResp['IpLocal']= $objGn->getIpRemoto();
	
	if(file_exists(WEB_CONFIG))
	{
		$datosXml=simplexml_load_file(WEB_CONFIG);
		if($datosXml)
		{
			for($i=0;$i<count($datosXml);$i++)
			{
				$sId=$datosXml->Config[$i]['categoria']->__toString();
				if(isset($datosXml->Config[$i]->elemento['habilitar']))
				{
					if($datosXml->Config[$i]->elemento['habilitar']=='SI')
					{
						$sValor= array('nombre'=>$datosXml->Config[$i]->elemento['nombre']->__toString(),'valor'=>$datosXml->Config[$i]->elemento->__toString()) ;
						$sApp=$datosXml->Config[$i]->elemento['nombre']->__toString();
						$arrResp[$sId][]=$sValor;
					}
				}
				else
				{
					$sValor= array('nombre'=>$datosXml->Config[$i]->elemento['nombre']->__toString(),'valor'=>$datosXml->Config[$i]->elemento->__toString()) ;
					$arrResp[$sId][]=$sValor;
				}
			}
			$arrResp['estado']=OK__;
		}
		else
		{
			$objGn->grabarLogx("formato XML invalido");
			$arrResp['estado']=ERR_FTO_XML;
		}
	}
	else
	{
		$arrResp['estado']=ERR_NO_EXISTE_ARCH;
		$objGn->grabarLogx("no existe XML");
	}
	echo json_encode($arrResp);
?>