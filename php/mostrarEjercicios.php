<?php
	include_once("CGenerales.php");
	include_once("JSON.php");
	include_once("global.php");
	$json = new Services_JSON();
	$arrRespuesta=array();
	$iEmpleado=0;
	$iKeyx=0;
	
	if($_POST['opcion'] == 1)
	{
		$iEmpleado=$_POST['empleado'];
		$arrRespuesta=consultarEjercicios($iEmpleado);
	}
	elseif($_POST['opcion'] == 2)
	{
		$iEmpleado=$_POST['empleado'];
		$iKeyx=$_POST['keyx'];
		$arrRespuesta=actualizarEjercicio($iEmpleado,$iKeyx);
	}
	else
	{
		$arrRespuesta['estado']=ERR_PARAM;
		$arrRespuesta['descripcion']=MSJ_ERR_PARAM;
	}
	echo $json->encode($arrRespuesta);
	
	function consultarEjercicios($iEmpleado)
	{
		$arrDatos = array ();  
		$cSql = "";
		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IPCAPACITACIONAFORE.";port=5432;dbname=".BASEDEDATOSCAPACITACIONAFORE, USUARIOCAPACITACIONAFOREBD, PASSWORDCAPACITACIONAFORE);

			if($cnxBd)
			{
				$cSql = "SELECT turl,tfechaalta,trealizado,tresultado FROM funmovrpejercicios(2,'1900-01-01','1900-01-01',0,0,0,1,0,$iEmpleado);";
				
				$resulSet = $cnxBd->query($cSql);

				if($resulSet)
				{
					foreach($resulSet as $rs)
					{
						$objetogenerico['url'] = TRIM($rs['turl']);
						$objetogenerico['fechaalta'] = TRIM($rs['tfechaalta']);
						$objetogenerico['keyx'] = $rs['trealizado'];
						$objetogenerico['consecutivo'] = $rs['tresultado'];
						$arrDatos[] = $objetogenerico;
					}
				}
				else
				{
					$arrErr = $cnxBd->errorInfo(); 
					//$objGn->grabarLogx( 'Error en la consulta [fnmostrarcursosactualizacion]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				//$objGn->grabarLogx( 'Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		}
		catch (Exception $e)
		{
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
			//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		$cnxBd = null;
		$resultset = null;
		return $arrDatos;
	}
	function actualizarEjercicio($iEmpleado,$iKeyx)
	{
		$arrDatos = array ();  
		$cSql = "";
		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IPCAPACITACIONAFORE.";port=5432;dbname=".BASEDEDATOSCAPACITACIONAFORE, USUARIOCAPACITACIONAFOREBD, PASSWORDCAPACITACIONAFORE);

			if($cnxBd)
			{
				$cSql = "SELECT turl,tfechaalta,trealizado,tresultado FROM funmovrpejercicios(6,'1900-01-01','1900-01-01',0,0,0,1,$iKeyx,$iEmpleado);";

				$resulSet = $cnxBd->query($cSql);

				if($resulSet)
				{
					foreach($resulSet as $rs)
					{
						$objetogenerico['realizado'] = TRIM($rs['turl']);
						$arrDatos[] = $objetogenerico;
					}
				}
				else
				{
					$arrErr = $cnxBd->errorInfo(); 
					//$objGn->grabarLogx( 'Error en la consulta [fnmostrarcursosactualizacion]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				//$objGn->grabarLogx( 'Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		}
		catch (Exception $e)
		{
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
			//$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		$cnxBd = null;
		$resultset = null;
		return $arrDatos;
	}
?>