<?php

	include_once("CGenerales.php");
	include_once("JSON.php");
	$objGn = new CGenerales();
	$json = new Services_JSON();
	$arrpromotorvalido=array();
	$iEmpleado=0;
	$sIpRemoto='';
	if(isset($_POST['empleado']))
	{
		$sIpRemoto=$objGn->getIpRemoto();
		$iEmpleado=$_POST['empleado'];
		$arrpromotorvalido=consultarpromotorvalido($iEmpleado);
	}
	else
	{
		$arrpromotorvalido['estado']=ERR_PARAM;
		$arrpromotorvalido['descripcion']=MSJ_ERR_PARAM;
		$objGn->grabarLogx(MSJ_ERR_PARAM);
	}
	echo $json->encode($arrpromotorvalido);
	
	function consultarpromotorvalido($iEmpleado)
	{
		global $objGn;
		global $sIpRemoto;
		$respPromotorValido=array('estado'=>0, 'descripcion'=>'', 'promotorvalido'=>'');
		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IP_BD_AFO.";port=5432;dbname=".BD_AFO, USR_BD_AFO, PWD_BD_AFO);
			if($cnxBd)
			{
				$sSql="SELECT fnobtenerpromotorvalido AS irespuesta FROM fnobtenerpromotorvalido(".$iEmpleado.");";
				$objGn->grabarLogx($sSql);
				$resulSet = $cnxBd->query($sSql);
				if($resulSet) 
				{ 
					$respPromotorValido['estado'] = OK__;
					$respPromotorValido['descripcion'] = MSJ_EXITO;
					foreach($resulSet as $reg) 
					{
						$respPromotorValido['promotorvalido']=  trim($reg['irespuesta']);
					}
						
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$respPromotorValido['estado'] = ERR_EXEC_CON_SQL;
					$respPromotorValido['descripcion'] = MSJ_ERR_EXEC_CON_SQL;
					$objGn->grabarLogx(' Error consultar empleado: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				$respPromotorValido['estado'] = ERR_CNX_BD;
				$respPromotorValido['descripcion'] = MSJ_ERR_CNX_BD;
				$objGn->grabarLogx(' Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			$cnxBd = null;
		}
		catch(PDOException $ex)
		{
			$respPromotorValido['estado'] = ERR__;
			$respPromotorValido['descripcion'] = MSJ_EXCEP_;
			$objGn->grabarLogx($ex->getMessage());
		}
		
		return $respPromotorValido;
	}

?>