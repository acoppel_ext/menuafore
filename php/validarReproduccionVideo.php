<?php

	include_once("CGenerales.php");
	include_once("JSON.php");
	$objGn = new CGenerales();
	$json = new Services_JSON();
	$arrReproduccionVideo=array();
	$iEmpleado=0;
	$sIpRemoto='';
	$tipo=0;
	$datos=array();
	if(isset($_POST['empleado']))
	{
		$datos = explode("-", $_POST['empleado']);
		$sIpRemoto=$objGn->getIpRemoto();
		$iEmpleado=$datos[0];
		//$tipo==1 Consultar si se va a reproducir el video
		//$tipo==2 Actualizar la informacion en la BD de que ya se reprodujo el video
		$tipo=$datos[1];
		$arrReproduccionVideo=consultarReproduccionVideo($iEmpleado, $tipo);
	}
	else
	{
		$arrReproduccionVideo['estado']=ERR_PARAM;
		$arrReproduccionVideo['descripcion']=MSJ_ERR_PARAM;
		$objGn->grabarLogx(MSJ_ERR_PARAM);
	}
	echo $json->encode($arrReproduccionVideo);
	
	function consultarReproduccionVideo($iEmpleado, $tipo)
	{
		global $objGn;
		global $sIpRemoto;
		$respRepVideo=array('estado'=>0, 'descripcion'=>'', 'respuesta'=>'', 'id'=>'');
		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IP_BD_AFO.";port=5432;dbname=".BD_AFO, USR_BD_AFO, PWD_BD_AFO);
			if($cnxBd)
			{
				$sSql="SELECT iretorno, ivideo FROM fnvalidarreproduccionvideomodulo(".$iEmpleado.", '".$sIpRemoto."', ".$tipo.");";
				$objGn->grabarLogx($sSql);
				$resulSet = $cnxBd->query($sSql);
				if($resulSet) 
				{ 
					$respRepVideo['estado'] = OK__;
					$respRepVideo['descripcion'] = MSJ_EXITO;
					foreach($resulSet as $reg) 
					{
						$respRepVideo['respuesta'] =  trim($reg['iretorno']);
						$respRepVideo['id'] =  trim($reg['ivideo']);
					}
					$objGn->grabarLogx('[consultarReproduccionVideo]Respuesta -> ' . $respRepVideo['respuesta'] . ' - id -> ' . $respRepVideo['id']);
						
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$respRepVideo['estado'] = ERR_EXEC_CON_SQL;
					$respRepVideo['descripcion'] = MSJ_ERR_EXEC_CON_SQL;
					$objGn->grabarLogx('[consultarReproduccionVideo] Error consultar empleado: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				$respRepVideo['estado'] = ERR_CNX_BD;
				$respRepVideo['descripcion'] = MSJ_ERR_CNX_BD;
				$objGn->grabarLogx('[consultarReproduccionVideo] Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			$cnxBd = null;
		}
		catch(PDOException $ex)
		{
			$respRepVideo['estado'] = ERR__;
			$respRepVideo['descripcion'] = MSJ_EXCEP_;
			$objGn->grabarLogx($ex->getMessage());
		}
		
		return $respRepVideo;
	}

?>