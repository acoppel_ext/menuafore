<?php

	include_once("CGenerales.php");
	include_once("JSON.php");
	$objGn = new CGenerales();
	$json = new Services_JSON();
	$arrTiempoMonitor=array();
	$iEmpleado=0;
	$sIpRemoto='';
	if(isset($_POST['empleado']))
	{
		$sIpRemoto=$objGn->getIpRemoto();
		$iEmpleado=$_POST['empleado'];
		$arrTiempoMonitor=consultaTiempoMonitor($iEmpleado);
	}
	else
	{
		$arrTiempoMonitor['estado']=ERR_PARAM;
		$arrTiempoMonitor['descripcion']=MSJ_ERR_PARAM;
		$objGn->grabarLogx(MSJ_ERR_PARAM);
	}
	echo $json->encode($arrTiempoMonitor);
	
	function consultaTiempoMonitor($iEmpleado)
	{
		global $objGn;
		global $sIpRemoto;
		$respTiempoMonitor=array('estado'=>0, 'descripcion'=>'', 'tiempoMonitor'=>'');
		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IP_BD_AFO.";port=5432;dbname=".BD_AFO, USR_BD_AFO, PWD_BD_AFO);
			if($cnxBd)
			{
				$sSql="SELECT fnmonitorafiliacionpromotortime AS irespuesta FROM fnmonitorafiliacionpromotortime(".$iEmpleado.");";
				$objGn->grabarLogx($sSql);
				$resulSet = $cnxBd->query($sSql);
				if($resulSet) 
				{ 
					$respTiempoMonitor['estado'] = OK__;
					$respTiempoMonitor['descripcion'] = MSJ_EXITO;
					foreach($resulSet as $reg) 
					{
						$respTiempoMonitor['tiempoMonitor'] =  trim($reg['irespuesta']);
					}
						
				}
				else
				{
					$arrErr = $cnxBd->errorInfo();
					$respTiempoMonitor['estado'] = ERR_EXEC_CON_SQL;
					$respTiempoMonitor['descripcion'] = MSJ_ERR_EXEC_CON_SQL;
					$objGn->grabarLogx('[consultaTiempoMonitor] Error consultar empleado: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$arrErr = $cnxBd->errorInfo();
				$respTiempoMonitor['estado'] = ERR_CNX_BD;
				$respTiempoMonitor['descripcion'] = MSJ_ERR_CNX_BD;
				$objGn->grabarLogx('[consultaTiempoMonitor] Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			$cnxBd = null;
		}
		catch(PDOException $ex)
		{
			$respTiempoMonitor['estado'] = ERR__;
			$respTiempoMonitor['descripcion'] = MSJ_EXCEP_;
			$objGn->grabarLogx($ex->getMessage());
		}
		
		return $respTiempoMonitor;
	}

?>